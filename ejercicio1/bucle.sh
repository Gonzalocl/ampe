#!/bin/bash -ux





if [ $# -lt 2 ]
then
	echo 'Usage: '$0' times exec'
	exit 1
fi

iters=$1
shift

#echo $*
#exit


for i in $(seq 1 $iters)
do
  $*
done
