#!/bin/bash -u

if [ $# -lt 2 ]
then
	echo 'Usage: '$0' times exec'
	exit 1
fi

ls $2 > /dev/null 2> /dev/null
if [ $? -ne 0 ]
then
	echo 'Specified file does not exist'
	exit 1
fi

echo 'Running...'
file=$(mktemp)
count=0

for c in $(seq 1 $1)
do
	echo "$c/$1"

	./$2 ${@:3} > $file

	count=$( echo $( cat $file) + $count | bc)
	cat $file
	echo $count
done

count=$( echo $count*1000/$1 | bc)
echo 'Mean: '$count' ms'

rm $file
