#include <fcntl.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

#define ECHO              "/sys/class/gpio/gpio45/value"
#define ECHO_DIRECTION    "/sys/class/gpio/gpio45/direction"
#define ECHO_EDGE         "/sys/class/gpio/gpio45/edge"

#define TRIGGER           "/sys/class/gpio/gpio44/value"
#define TRIGGER_DIRECTION "/sys/class/gpio/gpio44/direction"
#define TRIGGER_EDGE      "/sys/class/gpio/gpio44/edge"

int main() {
  int i;

  int fd_echo;
  int fd_echo_dir;
  int fd_echo_edge;

  int fd_trig;
  int fd_trig_dir;
  int fd_trig_edge;

  int epollfd;
  struct epoll_event ev;

  if ((epollfd = epoll_create(1)) == -1) {
    perror("epoll_create");
    return -1;
  }

  /* GPIO files */
  if ((fd_echo = open(ECHO, O_RDONLY | O_NONBLOCK)) == -1) {
    perror("open");
    return -1;
  }

  if ((fd_echo_dir = open(ECHO_DIRECTION, O_WRONLY | O_NONBLOCK)) == -1) {
    perror("open");
    return -1;
  }

  if ((fd_echo_edge = open(ECHO_EDGE, O_WRONLY | O_NONBLOCK)) == -1) {
    perror("open");
    return -1;
  }

  if ((fd_trig = open(TRIGGER, O_WRONLY)) == -1) {
    perror("open");
    return -1;
  }
  if ((fd_trig_dir = open(TRIGGER_DIRECTION, O_WRONLY)) == -1) {
    perror("open");
    return -1;
  }
  if ((fd_trig_edge = open(TRIGGER_EDGE, O_WRONLY)) == -1) {
    perror("open");
    return -1;
  }

  /* Make sure pins are initialized */
  if(write(fd_trig_dir,"out",3) == -1) {
    perror("write: fd_trig_dir");
    return -1;
  }

  if(write(fd_trig_edge,"none",4) == -1) {
    perror("write: fd_trig_edge");
    return -1;
  }

  if(write(fd_echo_dir,"in",2) == -1) {
    perror("write: fd_trig_edge");
    return -1;
  }

  if(write(fd_echo_edge,"both",4) == -1) {
    perror("write: fd_trig_edge");
    return -1;
  }

  if(close(fd_trig_dir) == -1) {
    perror("close");
    return 1;
  }

  if(close(fd_trig_edge) == -1) {
    perror("close");
    return 1;
  }

  if(close(fd_echo_dir) == -1) {
    perror("close");
    return 1;
  }

  if(close(fd_echo_edge) == -1) {
    perror("close");
    return 1;
  }

  ev.events = EPOLLIN | EPOLLET | EPOLLPRI;
  ev.data.fd = fd_echo;

  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd_echo, &ev) == -1) {
    perror("epoll_ctl");
    return -1;
  }

  i = epoll_wait(epollfd, &ev, 1, -1);

  /* Write */

  if(write(fd_trig, "0", 1) == -1) {
    perror("write: fd_trig");
    return -1;
  }

  usleep(2);

  if(write(fd_trig, "1", 1) == -1) {
    perror("write: fd_trig");
    return -1;
  }

  usleep(5);

  if(write(fd_trig, "0", 1) == -1) {
    perror("write: fd_trig");
    return -1;
  }

  i = epoll_wait(epollfd, &ev, 1, -1);

  struct timeval t1, t2;
  gettimeofday(&t1, NULL);
  i = epoll_wait(epollfd, &ev, 1, -1);
  gettimeofday(&t2, NULL);

  //printf("%li usec\n", t2.tv_usec - t1.tv_usec);
  printf("%f cm\n", ((t2.tv_usec - t1.tv_usec)/2)/29.1f );
}
