#include <stdbool.h>
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#define FILE_IMG "img.jpg"

bool read_all(int fd, char* buf,unsigned int bytes_to_read) {
  unsigned long bytes_read = 0;
  long ret = 0;
  do {
    ret = read(fd,buf+bytes_read,bytes_to_read-bytes_read);
    if(ret == 0) {
      if(bytes_read != bytes_to_read) {
        fprintf(stderr, "read_all: Unexpected end of file\n");
        return false;
      }
      return true;
    }
    if(ret == -1) {
      perror("read_all: read");
      return false;
    }
    bytes_read+=(unsigned long)ret;
  } while(bytes_read != bytes_to_read);

  return true;
}

int write_all(int fdd, char* buf,unsigned int bytes_to_write) {
  unsigned long bytes_write = 0;
  long ret = 0;
  do {
    ret = write(fdd,buf+bytes_write,bytes_to_write-bytes_write);
    if(ret != -1)bytes_write+=(unsigned long)ret;
    else {
        perror("write_all: write");
        return -1;
    }
  } while(bytes_write != bytes_to_write);

  return 1;
}

bool writeImage(char* img, long size) {
	int fd;
  if((fd = open(FILE_IMG, O_WRONLY | O_CREAT)) == -1) {
		perror("open");
		return false;
	}

	return write_all(fd, img, (int)size) == 1;
}


int main(int argc, char* argv[]) {
  char* addrstr = "192.168.1.12";
  int port = 5001;
  int socketfd = -1;
  struct sockaddr_in serv_addr;

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  if(inet_pton(AF_INET, addrstr, &serv_addr.sin_addr) <= 0 ) {
    perror("inet_pton");
    return EXIT_FAILURE;
  }

  if((socketfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		return EXIT_FAILURE;
	}

  printf("Connecting to %s:%d\n",addrstr, port);

  if(connect(socketfd, (struct sockaddr *)&serv_addr, sizeof(struct sockaddr_in)) < 0) {
    perror("connect");
    return EXIT_FAILURE;
  }

  printf("Connection established\n");

	write_all(socketfd, "1", 1);

  long img_size;
  if(!read_all(socketfd, (char *)&img_size, sizeof(long)))
    return EXIT_FAILURE;

	printf("img_size = %li\n",img_size);

	char* buf = malloc(sizeof(char)* img_size);
  if(!read_all(socketfd, buf, img_size))
    return EXIT_FAILURE;

  if(!writeImage(buf, img_size))
   return EXIT_FAILURE;
}
