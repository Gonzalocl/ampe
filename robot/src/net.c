#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>

int open_port(int port, char* fun_name) {
  int listenfd = -1;
  int socketfd = -1;
	static struct sockaddr_in cli_addr;
	static struct sockaddr_in serv_addr;
  socklen_t length = sizeof(cli_addr);

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  serv_addr.sin_port = htons(port);

  if((listenfd = socket(AF_INET, SOCK_STREAM,0)) == -1) {
    printf("%s: ",fun_name);
    fflush(stdout);
		perror("socket");
		return -1;
	}

  /*
   reuse port, avoiding "address already in use" 
  if(setsockopt(listenfd, SOL_SOCKET, SO_REUSEPORT, &(int){ 1 }, sizeof(int)) == -1) {
    printf("%s: ",fun_name);
    fflush(stdout);
		perror("socket");
		return -1;
  }
  */

  if(bind(listenfd, (struct sockaddr *)&serv_addr,sizeof(serv_addr)) == -1) {
    printf("%s: ",fun_name);
    fflush(stdout);
		perror("bind");
		return -1;
	}

  if(listen(listenfd, 0) == -1) {
    printf("%s: ",fun_name);
    fflush(stdout);
		perror("listen");
		return -1;
	}

  if((socketfd = accept(listenfd, (struct sockaddr *)&cli_addr, &length)) < 0) {
    printf("%s: ",fun_name);
    fflush(stdout);
		perror("accept");
    return -1;
  }

  return socketfd;
}
