#include <stdio.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <sys/time.h>

#include "motor.h"

#define PATH "/sys/class/gpio/gpio3/value"
#define EDGE "/sys/class/gpio/gpio3/edge"

int main() {
  int i;
  int fd;
  int fd_edge;
  int epollfd;
  struct epoll_event ev;

  if ((epollfd = epoll_create(1)) == -1) {
    perror("epoll_create");
    return -1;
  }

  if ((fd = open(PATH, O_RDONLY | O_NONBLOCK)) == -1) {
    perror("open");
    return -1;
  }
  if ((fd_edge = open(EDGE, O_WRONLY | O_NONBLOCK)) == -1) {
    perror("open");
    return -1;
  }

  if(write(fd_edge, "both", 4) == -1) {
    perror("write");
    return -1;
  }

  ev.events = EPOLLIN | EPOLLET | EPOLLPRI;
  ev.data.fd = fd;

  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
    perror("epoll_ctl");
    return -1;
  }
  
  i = epoll_wait(epollfd, &ev, 1, -1);
  init();  
  printf("motor done\n");

  int c = 0;
  long dif = 0;
  struct timeval t1,t2;
  gettimeofday(&t1,NULL);
  l_set_speed(-1);
 
  while(dif <= 2) {
    gettimeofday(&t2,NULL);
    i = epoll_wait(epollfd, &ev, 1, 1000);
    if (i == -1){
      perror("GPIO: Poll Wait fail");
    }
    else if(i != 0) {
      c++;
    }
    dif = t2.tv_sec - t1.tv_sec;
  }
  l_set_speed(0);

  printf("%d ticks\n",c); 
}
