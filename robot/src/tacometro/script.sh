pwm:
/sys/devices/platform/ocp/ocp:P9_16_pinmux/state pwm
/sys/class/pwm/pwm-4:1/period                    5000
/sys/class/pwm/pwm-4:1/duty_cycle                4000
/sys/class/pwm/pwm-4:1/enable                    1

gpio:
/sys/devices/platform/ocp/ocp:P9_27_pinmux/state gpio
/sys/devices/platform/ocp/ocp:P9_12_pinmux/state gpio
/sys/class/gpio/gpio60/direction                 out
/sys/class/gpio/gpio60/value                     1
/sys/class/gpio/gpio115/direction                out
/sys/class/gpio/gpio115/value                    0
