#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <errno.h>

#include "motor.h"

#define OP_END   '0'
#define OP_RIGHT '1'
#define OP_LEFT  '2'

#define PORT 6464

#define PERIOD 5000
#define DUTY_MAX 3900
#define DUTY_MIN 1100

#define GPIO_1  "48"
#define GPIO_2  "49"
#define GPIO_3  "60"
#define GPIO_4 "115"

int pwm_A, pwm_B, in_A1, in_A2, in_B1, in_B2;

void fun(char* path, char* data, int size) {
  int fd;

  if((fd = open(path, O_WRONLY)) == -1) {
    printf("%s === %s\n",path, data);
    perror("open");
    return;
  }
  if(write(fd, data, size) == -1) {
    printf("%s === %s\n",path, data);
    perror("write");
    return;
  }
  if(close(fd) == -1) {
    printf("%s === %s\n",path, data);
    perror("close");
    return;
  }
}

void init() {
  struct stat sb;

  //pwm_A
  fun("/sys/devices/platform/ocp/ocp:P9_14_pinmux/state", "pwm", 3);
  if(stat("/sys/class/pwm/pwm-4:0", &sb) == ENOENT)
    fun("/sys/class/pwm/pwmchip4/export", "0", 1);
  fun("/sys/class/pwm/pwm-4:0/period", "5000", 4);
  fun("/sys/class/pwm/pwm-4:0/duty_cycle", "0", 1);
  fun("/sys/class/pwm/pwm-4:0/enable", "1", 1);

  //pwm_B
  fun("/sys/devices/platform/ocp/ocp:P9_16_pinmux/state", "pwm", 3);
  if(stat("/sys/class/pwm/pwm-4:1", &sb) == ENOENT)
    fun("/sys/class/pwm/pwmchip4/export", "1", 1);
  fun("/sys/class/pwm/pwm-4:1/period", "5000", 4);
  fun("/sys/class/pwm/pwm-4:1/duty_cycle", "0", 1);
  fun("/sys/class/pwm/pwm-4:1/enable", "1", 1);

  //in_A1
  fun("/sys/devices/platform/ocp/ocp:P9_15_pinmux/state", "gpio", 4);
  if(stat("/sys/class/gpio/gpio" GPIO_1, &sb) == ENOENT)
    fun("/sys/class/gpio/export", GPIO_1, 2);
  fun("/sys/class/gpio/gpio" GPIO_1 "/direction", "out", 3);
  fun("/sys/class/gpio/gpio" GPIO_1 "/value", "0", 1);

  //in_A2
  fun("/sys/devices/platform/ocp/ocp:P9_23_pinmux/state", "gpio", 4);
  if(stat("/sys/class/gpio/gpio" GPIO_2, &sb) == ENOENT)
    fun("/sys/class/gpio/export", GPIO_2, 2);
  fun("/sys/class/gpio/gpio" GPIO_2 "/direction", "out", 3);
  fun("/sys/class/gpio/gpio" GPIO_2 "/value", "0", 1);

  //in_B1
  fun("/sys/devices/platform/ocp/ocp:P9_12_pinmux/state", "gpio", 4);
  if(stat("/sys/class/gpio/gpio" GPIO_3, &sb) == ENOENT)
    fun("/sys/class/gpio/export", GPIO_3, 2);
  fun("/sys/class/gpio/gpio" GPIO_3 "/direction", "out", 3);
  fun("/sys/class/gpio/gpio" GPIO_3 "/value", "0", 1);

  //in_B2
  fun("/sys/devices/platform/ocp/ocp:P9_27_pinmux/state", "gpio", 4);
  if(stat("/sys/class/gpio/gpio" GPIO_4, &sb) == ENOENT)
    fun("/sys/class/gpio/export", GPIO_4, 3);
  fun("/sys/class/gpio/gpio" GPIO_4 "/direction", "out", 3);
  fun("/sys/class/gpio/gpio" GPIO_4 "/value", "0", 1);
}




bool r_direction = true;
bool l_direction = true;

void r_set_direction(float d) {
  in_A1 = open("/sys/class/gpio/gpio48/value", O_WRONLY);
  in_A2 = open("/sys/class/gpio/gpio49/value", O_WRONLY);

  if ( r_direction && d < 0 ) {
    write(in_A1, "0", 1);
    write(in_A2, "1", 1);
    r_direction = false;
  }
  else if ( !r_direction && d > 0 ) {
    write(in_A2, "0", 1);
    write(in_A1, "1", 1);
    r_direction = true;
  }

  close(in_A1);
  close(in_A2);
}
void l_set_direction(float d) {
  if((in_B1 = open("/sys/class/gpio/gpio60/value", O_WRONLY)) == -1) perror("open");
  if((in_B2 = open("/sys/class/gpio/gpio115/value", O_WRONLY)) == -1) perror("open");

  if ( l_direction && d < 0 ) {
    if(write(in_B1, "0", 1) == -1)perror("write");
    if(write(in_B2, "1", 1) == -1)perror("write");
    l_direction = false;
  }
  else if ( !l_direction && d > 0 ) {
    if(write(in_B2, "0", 1) == -1)perror("write");
    if(write(in_B1, "1", 1) == -1)perror("write");
    l_direction = true;
  }
  if(close(in_B1) == -1)perror("close");
  if(close(in_B2) == -1)perror("close");
}

void r_set_speed(float s) {
  r_set_direction(s);
  if (s < 0) s = s * -1;
  char str[16];
  int len = sprintf(str, "%d", (int)((float)DUTY_MIN + (s*((float)DUTY_MAX-(float)DUTY_MIN))));
  pwm_A = open("/sys/class/pwm/pwm-4:0/duty_cycle", O_WRONLY);
  write(pwm_A, str, len);
  close(pwm_A);
}

void l_set_speed(float s) {
  l_set_direction(s);
  if (s < 0) s = s * -1;
  char str[16];
  int len = sprintf(str, "%d", (int)((float)DUTY_MIN + (s*((float)DUTY_MAX-(float)DUTY_MIN))));

  fun("/sys/class/pwm/pwm-4:1/duty_cycle",str,len);
}

