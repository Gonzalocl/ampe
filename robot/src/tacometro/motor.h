#ifndef __MOTOR_TH__
#define __MOTOR_TH__

struct th_st_motor {
  int port;
};

void* thread_motor(void* args);
void init();
void r_set_speed(float s);
void l_set_speed(float s);

#endif
