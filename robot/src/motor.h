#ifndef __MOTOR_TH__
#define __MOTOR_TH__

struct th_st_motor {
  int port;
};

void* thread_motor(void* args);

#endif
