#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>

#include "cap.h"
#include "motor.h"
#include "ultrasonic.h"
#include "net.h"

#define OP_END '0'

#define PORT_CONTROL    5000
#define PORT_CAMERA     5001
#define PORT_MOTOR      5002
#define PORT_ULTRASONIC 5003

#define THREAD_CAMERA     0
#define THREAD_MOTOR      1
#define THREAD_ULTRASONIC 2

/*
  Hay 3 tipos de hilos:
  - El hilo principal: Acaba cuando lee un 0 por el socket y
    crea los demas hilos
  - Los hilos que leen de la app: Leen su entrada por el socket,
    respondiendo con datos a la app y terminan cuando leen un 0
  - Los hilos que envian a la app: Escriben las salidas de los
    sensores a la app por el socket, y terminan cuando el hilo
    maestro lo indica
*/

int main(int argc, char *argv[]) {
  int socketfd;
  bool must_end = false;
  int port = PORT_CONTROL;
  int ret;
  char op;

  /* prepare thread structs */
  pthread_t threads[3];
  struct th_st_image st_image;
  struct th_st_motor st_motor;
  struct th_st_ultrasonic st_ultrasonic;
  st_image.port = PORT_CAMERA;
  st_motor.port = PORT_MOTOR;
  st_ultrasonic.port = PORT_ULTRASONIC;
  st_ultrasonic.must_end = &must_end;

  /* Create threads */
  if(pthread_create(&threads[THREAD_CAMERA], NULL, &thread_camera, &st_image) != 0) {
    perror("main: pthread_create");
    return EXIT_FAILURE;
  }

  if(pthread_create(&threads[THREAD_MOTOR], NULL, &thread_motor, &st_motor) != 0) {
    perror("main: pthread_create");
    return EXIT_FAILURE;
  }

  if(pthread_create(&threads[THREAD_ULTRASONIC], NULL, &thread_ultrasonic, &st_ultrasonic) != 0) {
    perror("main: pthread_create");
    return EXIT_FAILURE;
  }

  /* Listen on main port */
  printf("Listening on port %d...\n",port);
  if((socketfd = open_port(port, "main")) == -1)
    return EXIT_FAILURE;

  printf("main: connected\n");

  /* Main loop */
  do {
    if((ret = read(socketfd, &op, 1)) <= 0) {
      if(ret == -1)perror("main: read");
      else printf("main: unexpected connection closed\n");
    }
    else {
      if(op != OP_END)
        printf("main: unknown op: %d\n", op);
    }
  } while( ret > 0 && op != OP_END );

  /* Terminate threads */

  must_end = true;

  for(int i=0;i<3;i++) {
    if(pthread_join(threads[i], NULL) != 0) {
      perror("main: pthread_join");
      return EXIT_FAILURE;
    }
  }

  if(close(socketfd) == -1) {
    perror("main: close");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
