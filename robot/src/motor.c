#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>

#include "motor.h"
#include "net.h"

#define OP_END   '0'
#define OP_RIGHT '1'
#define OP_LEFT  '2'

#define PORT 6464

#define PERIOD 5000
#define DUTY_MAX 3900
#define DUTY_MIN 1100

#define GPIO_1  "48"
#define GPIO_2  "49"
#define GPIO_3  "60"
#define GPIO_4 "115"

int pwm_A, pwm_B, in_A1, in_A2, in_B1, in_B2;

void init() {
  //pwm_A
  int fd = open("/sys/devices/platform/ocp/ocp:P9_14_pinmux/state", O_WRONLY);
  write(fd, "pwm", 3);
  close(fd);

  fd = open("/sys/class/pwm/pwmchip4/export", O_WRONLY);
  write(fd, "0", 1);
  close(fd);

  fd = open("/sys/class/pwm/pwm-4:0/period", O_WRONLY);
  write(fd, "5000", 4);
  close(fd);

  pwm_A = open("/sys/class/pwm/pwm-4:0/duty_cycle", O_WRONLY);
  write(pwm_A, "0", 1);
  close(pwm_A);

  fd = open("/sys/class/pwm/pwm-4:0/enable", O_WRONLY);
  write(fd, "1", 1);
  close(fd);

  //pwm_B
  fd = open("/sys/devices/platform/ocp/ocp:P9_16_pinmux/state", O_WRONLY);
  write(fd, "pwm", 3);
  close(fd);

  fd = open("/sys/class/pwm/pwmchip4/export", O_WRONLY);
  write(fd, "1", 1);
  close(fd);

  fd = open("/sys/class/pwm/pwm-4:1/period", O_WRONLY);
  write(fd, "5000", 4);
  close(fd);

  pwm_B = open("/sys/class/pwm/pwm-4:1/duty_cycle", O_WRONLY);
  write(pwm_B, "0", 1);

  fd = open("/sys/class/pwm/pwm-4:1/enable", O_WRONLY);
  write(fd, "1", 1);
  close(fd);


  //in_A1
  fd = open("/sys/devices/platform/ocp/ocp:P9_15_pinmux/state", O_WRONLY);
  write(fd, "gpio", 4);
  close(fd);

  fd = open("/sys/class/gpio/unexport", O_WRONLY);
  write(fd, GPIO_1, 2);
  close(fd);

  fd = open("/sys/class/gpio/export", O_WRONLY);
  write(fd, GPIO_1, 2);
  close(fd);

  fd = open("/sys/class/gpio/gpio" GPIO_1 "/direction", O_WRONLY);
  write(fd, "out", 3);
  close(fd);

  in_A1 = open("/sys/class/gpio/gpio" GPIO_1 "/value", O_WRONLY);
  write(in_A1, "0", 1);
  close(in_A1);


  //in_A2
  fd = open("/sys/devices/platform/ocp/ocp:P9_23_pinmux/state", O_WRONLY);
  write(fd, "gpio", 4);
  close(fd);

  fd = open("/sys/class/gpio/unexport", O_WRONLY);
  write(fd, GPIO_2, 2);
  close(fd);

  fd = open("/sys/class/gpio/export", O_WRONLY);
  write(fd, GPIO_2, 2);
  close(fd);

  fd = open("/sys/class/gpio/gpio" GPIO_2 "/direction", O_WRONLY);
  write(fd, "out", 3);
  close(fd);

  in_A2 = open("/sys/class/gpio/gpio" GPIO_2 "/value", O_WRONLY);
  write(in_A2, "0", 1);
  close(in_A2);


  //in_B1
  fd = open("/sys/devices/platform/ocp/ocp:P9_12_pinmux/state", O_WRONLY);
  write(fd, "gpio", 4);
  close(fd);

  fd = open("/sys/class/gpio/unexport", O_WRONLY);
  write(fd, GPIO_3, 2);
  close(fd);

  fd = open("/sys/class/gpio/export", O_WRONLY);
  write(fd, GPIO_3, 2);
  close(fd);

  fd = open("/sys/class/gpio/gpio" GPIO_3 "/direction", O_WRONLY);
  write(fd, "out", 3);
  close(fd);

  in_B1 = open("/sys/class/gpio/gpio" GPIO_3 "/value", O_WRONLY);
  write(in_B1, "0", 1);


  //in_B2
  fd = open("/sys/devices/platform/ocp/ocp:P9_27_pinmux/state", O_WRONLY);
  write(fd, "gpio", 4);
  close(fd);

  fd = open("/sys/class/gpio/unexport", O_WRONLY);
  write(fd, GPIO_4, 3);
  close(fd);

  fd = open("/sys/class/gpio/export", O_WRONLY);
  write(fd, GPIO_4, 3);
  close(fd);

  fd = open("/sys/class/gpio/gpio" GPIO_4 "/direction", O_WRONLY);
  write(fd, "out", 3);
  close(fd);

  in_B2 = open("/sys/class/gpio/gpio" GPIO_4 "/value", O_WRONLY);
  write(in_B2, "0", 1);
}




bool r_direction = true;
bool l_direction = true;

void r_set_direction(float d) {
  in_A1 = open("/sys/class/gpio/gpio48/value", O_WRONLY);
  in_A2 = open("/sys/class/gpio/gpio49/value", O_WRONLY);
  if ( r_direction && d < 0 ) {
    write(in_A1, "0", 1);
    write(in_A2, "1", 1);
    r_direction = false;
  }
  else if ( !r_direction && d > 0 ) {
    write(in_A2, "0", 1);
    write(in_A1, "1", 1);
    r_direction = true;
  }
  close(in_A1);
  close(in_A2);
}
void l_set_direction(float d) {
  in_B1 = open("/sys/class/gpio/gpio60/value", O_WRONLY);
  in_B2 = open("/sys/class/gpio/gpio115/value", O_WRONLY);
  if ( l_direction && d < 0 ) {
    write(in_B1, "0", 1);
    write(in_B2, "1", 1);
    l_direction = false;
  }
  else if ( !l_direction && d > 0 ) {
    write(in_B2, "0", 1);
    write(in_B1, "1", 1);
    l_direction = true;
  }
  close(in_B1);
  close(in_B2);
}

void r_set_speed(float s) {
  r_set_direction(s);
  if (s < 0) s = s * -1;
  char str[16];
  int len = sprintf(str, "%d", (int)((float)DUTY_MIN + (s*((float)DUTY_MAX-(float)DUTY_MIN))));
  pwm_A = open("/sys/class/pwm/pwm-4:0/duty_cycle", O_WRONLY);
  write(pwm_A, str, len);
  close(pwm_A);
}

void l_set_speed(float s) {
  l_set_direction(s);
  if (s < 0) s = s * -1;
  char str[16];
  int len = sprintf(str, "%d", (int)((float)DUTY_MIN + (s*((float)DUTY_MAX-(float)DUTY_MIN))));
  pwm_B = open("/sys/class/pwm/pwm-4:1/duty_cycle", O_WRONLY);
  write(pwm_B, str, len);
  close(pwm_B);
}



void* thread_motor(void* args) {
  init();
  struct th_st_motor *st = (struct th_st_motor *)args;

  int port = st->port;
  int socketfd;
  printf("thread_motor:      listening on %d\n", port);

  if((socketfd = open_port(port, "thread_motor")) == -1)
    return NULL;

  printf("thread_motor: connected\n");

  float x;
  char op;
  int ret;

  do {
    if((ret = read(socketfd, &op, 1)) <= 0) {
      if(ret == -1)perror("thread_motor: read");
      else printf("thread_motor: unexpected connection closed\n");
    }
    else {
      if (op == OP_RIGHT) {
        char* buf = (char*)&x;
        read(socketfd, buf+3, 1);
        read(socketfd, buf+2, 1);
        read(socketfd, buf+1, 1);
        read(socketfd, buf, 1);
        //read(socketfd, &op, 1);
	

	printf("Speed R: %f\n", x);
        r_set_speed(x);
      }
      else if (op == OP_LEFT){
        char* buf = (char*)&x;
        read(socketfd, buf+3, 1);
        read(socketfd, buf+2, 1);
        read(socketfd, buf+1, 1);
        read(socketfd, buf, 1);
        //read(socketfd, &op, 1);

	printf("Speed L: %f\n", x);
        l_set_speed(x);
      }
      else if(op != OP_END)
        printf("thread_motor: unknown op: %d\n", op);
    }
  } while( ret > 0 && op != OP_END );

  printf("thread_motor: exiting\n");

  if(close(socketfd) == -1)
    perror("thread_motor: close");

  return NULL;
}
