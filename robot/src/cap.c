#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <jpeglib.h>
#include <libv4l2.h>
#include <stdbool.h>
#include <unistd.h>

#include "cap.h"
#include "net.h"

#define OP_END '0'
#define OP_CAP '1'

struct buffer {
  void * start;
  long length;
};

static int              fd              = -1;
struct buffer *         buffers         = NULL;
static unsigned int     n_buffers       = 0;

// global settings
static unsigned int width        = 320;
static unsigned int height       = 240;
static unsigned char jpegQuality = 100;
static char* deviceName          = "/dev/video0";

int write_all(int fdd, char* buf,unsigned int bytes_to_write) {
  unsigned long bytes_write = 0;
  long ret = 0;
  do {
    ret = write(fdd,buf+bytes_write,bytes_to_write-bytes_write);
    if(ret != -1)bytes_write+=(unsigned long)ret;
    else {
        perror("write_all: write");
        return (int)-errno;
    }
  } while(bytes_write != bytes_to_write);

  return 1;
}

void YUV420toYUV444(unsigned char* src, unsigned char* dst) {
	unsigned int line, column;
	unsigned char *py, *pu, *pv;
	unsigned char *tmp = dst;

	// In this format each four bytes is two pixels. Each four bytes is two Y's, a Cb and a Cr.
	// Each Y goes to one of the pixels, and the Cb and Cr belong to both pixels.
	unsigned char *base_py = src;
	unsigned char *base_pu = src+(height*width);
	unsigned char *base_pv = src+(height*width)+(height*width)/4;

	for (line = 0; line < height; ++line) {
		for (column = 0; column < width; ++column) {
			py = base_py+(line*width)+column;
			pu = base_pu+(line/2*width/2)+column/2;
			pv = base_pv+(line/2*width/2)+column/2;

			*tmp++ = *py;
			*tmp++ = *pu;
			*tmp++ = *pv;
		}
	}
}

/**
	Do ioctl and retry if error was EINTR ("A signal was caught during the ioctl() operation."). Parameters are the same as on ioctl.

	\param fd file descriptor
	\param request request
	\param argp argument
	\returns result from ioctl
*/
int xioctl(long unsigned int request, void* argp) {
	int r;

	do {
    r = v4l2_ioctl(fd, request, argp);
	} while (r == -1 && errno == EINTR);

	return r;
}

/**
	Write image to jpeg file.

	\param img image to write
*/
struct buffer* jpegWriteBuf(unsigned char* img) {
  struct buffer* buf_s;
	struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr       jerr;

  unsigned char* buf = NULL;
  unsigned long s = 0;

	JSAMPROW row_pointer[1];

  cinfo.err = jpeg_std_error( &jerr );
	jpeg_create_compress(&cinfo);
	jpeg_mem_dest(&cinfo, &buf, &s);

	// set image parameters
  cinfo.image_width = width & -1;
  cinfo.image_height = height & -1;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_YCbCr;

	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, jpegQuality, TRUE);
	jpeg_start_compress(&cinfo, TRUE);

	// feed data
	while (cinfo.next_scanline < cinfo.image_height) {
		row_pointer[0] = &img[cinfo.next_scanline * cinfo.image_width *  cinfo.input_components];
		jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);

  buf_s = malloc(sizeof(struct buffer));
  buf_s->start = buf;
  buf_s->length = s;

  return buf_s;
}

/**
	process image read
*/
struct buffer* imageProcess(const void* p) {
  struct buffer* ret;
	unsigned char* src = (unsigned char*)p;
	unsigned char* dst = malloc(width*height*3*sizeof(char));

	YUV420toYUV444(src, dst);
	ret = jpegWriteBuf(dst);

	free(dst);
  return ret;
}

bool imageSend(int socketfd, long length, void* p) {
  printf("Send %li bytes\n",length);
  long length_be = htole64(length);

  write_all(socketfd, (char *)&length_be, sizeof(long));
  write_all(socketfd, (char *)p, length);


  return true;
}

bool sendFrame(int socketfd) {
  struct buffer* img_buf = NULL;
  struct v4l2_buffer buf;

  memset(&buf, 0, sizeof(buf));
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;

  /* Aqui es donde realmente se lee */
  if (xioctl(VIDIOC_DQBUF, &buf) == -1) {
    fprintf(stderr, "sendFrame: xioctl VIDIOC_DQBUF");
		perror("");
    return false;
  }

  assert(buf.index < n_buffers);

  if((img_buf = imageProcess(buffers[buf.index].start)) == NULL)
    return false;

  if(!imageSend(socketfd, img_buf->length, img_buf->start))
    return false;

  /* Despues de leer hay que llamar a
  xioctl con VIDIOC_QBUF*/
  if (xioctl(VIDIOC_QBUF, &buf) == -1) {
    fprintf(stderr, "sendFrame: xioctl VIDIOC_QBUF");
		perror("");
    return false;
  }

  free(img_buf->start);
  free(img_buf);

	return true;
}

bool captureStop() {
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

  if (xioctl(VIDIOC_STREAMOFF, &type) == -1) {
    fprintf(stderr, "captureStop: xioctl VIDIOC_STREAMOFF: ");
    perror("");
    return false;
  }

  return true;
}

bool captureStart() {
	enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  struct v4l2_buffer buf;

	for (unsigned int i = 0; i < n_buffers; i++) {
		memset(&buf, 0, sizeof(buf));

		buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index  = i;

		if (xioctl(VIDIOC_QBUF, &buf) == -1) {
      fprintf(stderr, "captureStart: xioctl VIDIOC_QBUF: ");
  		perror("");
      return false;
    }
	}

	if (xioctl(VIDIOC_STREAMON, &type) == -1) {
    fprintf(stderr, "captureStart: xioctl VIDIOC_STREAMON: ");
    perror("");
    return false;
  }

  return true;
}

bool deviceUninit() {
	for (unsigned int i = 0; i < n_buffers; ++i) {
		if (v4l2_munmap(buffers[i].start, buffers[i].length) == -1) {
      fprintf(stderr, "deviceUninit: v4l2_munmap: ");
      perror("");
      return false;
    }
  }
	free(buffers);

  return true;
}

bool mmapInit() {
  struct v4l2_buffer buf;
	struct v4l2_requestbuffers req;
	memset(&req, 0, sizeof(req));

	req.count  = 2; /* aqui antes habia 2 (para que?)*/
	req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	if (xioctl(VIDIOC_REQBUFS, &req) == -1) {
    fprintf(stderr, "mmapInit: xioctl VIDIOC_REQBUFS: ");
		perror("");
    return false;
	}

	if((buffers = malloc(req.count * sizeof(*buffers))) == NULL) {
    fprintf(stderr, "mmapInit: malloc: ");
    perror("");
    return false;
  }
  memset(buffers, 0 , req.count * sizeof(*buffers));

	for (n_buffers = 0; n_buffers < req.count; n_buffers++) {
		memset(&buf, 0, sizeof(buf));

		buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index  = n_buffers;

		if (xioctl(VIDIOC_QUERYBUF, &buf) == -1) {
      fprintf(stderr, "mmapInit: xioctl VIDIOC_QUERYBUF: ");
      perror("");
      return false;
    }

		buffers[n_buffers].length = buf.length;
		buffers[n_buffers].start = v4l2_mmap(NULL, buf.length, PROT_READ | PROT_WRITE , MAP_SHARED, fd, buf.m.offset);

		if (buffers[n_buffers].start == MAP_FAILED) {
      fprintf(stderr, "mmapInit: v4l2_mmap failed");
      return false;
    }
	}

  return true;
}

/**
	initialize device
*/
bool deviceInit() {
  struct v4l2_capability  cap;
	struct v4l2_cropcap     cropcap;
	struct v4l2_crop        crop;
	struct v4l2_format      fmt;
	struct v4l2_streamparm  frameint;
	unsigned int min;

  memset(&cropcap, 0, sizeof(cropcap));
  memset(&fmt, 0, sizeof(fmt));

	if (xioctl(VIDIOC_QUERYCAP, &cap) == -1) {
    fprintf(stderr, "deviceInit: xioctl VIDIOC_QUERYCAP: ");
		perror("");
    return false;
	}

	if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		fprintf(stderr, "deviceInit: '%s' is not a video capture device\n",deviceName);
		return false;
	}

  if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
    fprintf(stderr, "deviceInit: '%s' does not supsocketfd streaming\n",deviceName);
		return false;
	}

	/* Select video input, video standard and tune here. */
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	if (xioctl(VIDIOC_CROPCAP, &cropcap) == -1) {
    fprintf(stderr, "deviceInit: xioctl VIDIOC_CROPCAP: ");
    perror("");
	}

  crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  crop.c = cropcap.defrect;

  if (xioctl(VIDIOC_S_CROP, &crop) == -1) {
    fprintf(stderr, "deviceInit: xioctl VIDIOC_S_CROP: ");
    perror("");
  }

	// v4l2_format
	fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width       = width;
	fmt.fmt.pix.height      = height;
	fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;

	if (xioctl(VIDIOC_S_FMT, &fmt) == -1) {
    fprintf(stderr, "deviceInit: xioctl VIDIOC_S_FMT: ");
    perror("");
    return false;
  }

	if (fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_YUV420) {
    fprintf(stderr, "deviceInit: V4L2 did not accept YUV420 format\n");
		return false;
	}

	/* VIDIOC_S_FMT may change width and height. */
	if (width != fmt.fmt.pix.width) {
		width = fmt.fmt.pix.width;
		printf("deviceInit: Image width set to %d by device %s\n", width, deviceName);
	}

	if (height != fmt.fmt.pix.height) {
		height = fmt.fmt.pix.height;
		printf("deviceInit Image height set to %d by device %s\n", height, deviceName);
	}

  return mmapInit();
}

bool deviceClose() {
  if (v4l2_close(fd) == -1) {
    fprintf(stderr, "deviceClose: v4l2_close '%d': ",fd);
		perror("");
		return false;
  }

  return true;
}

bool deviceOpen() {
	struct stat st;

	if (stat(deviceName, &st) == -1) {
    fprintf(stderr, "deviceOpen: stat '%s': ",deviceName);
		perror("");
		return false;
	}

	if (!S_ISCHR(st.st_mode)) {
		fprintf(stderr, "deviceOpen: stat '%s': not a character device\n",deviceName);
		return false;
	}

	if ((fd = v4l2_open(deviceName, O_RDWR | O_NONBLOCK, 0)) == -1) {
    fprintf(stderr, "deviceOpen: v4l2_open '%s': ",deviceName);
		perror("");
		return false;
	}

  return true;
}

/*
 * Thread interface
 */

bool cap_init() {
  printf("Trying %dx%d resolution\n",width,height);

	if(!deviceOpen())
    return false;

	if(!deviceInit())
    return false;

  if(!captureStart())
    return false;

  return true;
}

bool cap_send(int socketfd) {
  int r;
	unsigned int maxTimeouts = 1;
  bool sendCompleted = false;
  fd_set fds;

  /* Tiempo real:
  ¿Cuanto deberiamos esperar
  para recibir los datos?
  */
  struct timeval timeout;
  timeout.tv_sec = 1;
  timeout.tv_usec = 0;

	while(!sendCompleted && maxTimeouts > 0) {
    FD_ZERO(&fds);
    FD_SET(fd, &fds);

    r = select(fd + 1, &fds, NULL, NULL, &timeout);

    if (r == -1) {
      if (EINTR == errno) {
        fprintf(stderr, "mainLoop: select: ");
        perror("");
      }
    }
    else if (0 == r) {
      fprintf(stderr, "mainLoop: timeout ");
      maxTimeouts--;
    }
    else {
      if (sendFrame(socketfd))
        sendCompleted = true;
    }
  }

  return sendCompleted;
}

bool cap_end() {
  if(!captureStop())
    return false;

	if(!deviceUninit())
    return false;

	if(!deviceClose())
    return false;

  return true;
}

/*
 * Thread function
 */

void* thread_camera(void* args) {
  struct th_st_image *st = (struct th_st_image *)args;

  int port = st->port;
  int socketfd;
  printf("thread_camera:     listening on %d\n", port);

  if((socketfd = open_port(port, "thread_camera")) == -1)
    return NULL;

  printf("thread_camera: connected\n");

  /*
   * Init camera
  */

  char op;
  int ret;

  if(!cap_init()) {
    printf("thread_camera: exiting\n");

    if(close(socketfd) == -1)
      perror("thread_camera: close");

    socketfd = -1;

    return NULL;
  }


  /*
   * Send frames when requested
  */

  do {
    if((ret = read(socketfd, &op, 1)) <= 0) {
      if(ret == -1)perror("thread_camera: read");
      else printf("thread_camera: unexpected connection closed\n");
    }
    else {
      if(op == OP_CAP) {
        if(!cap_send(socketfd))
          printf("thread_camera: capture failed");
      }
      else if(op != OP_END)
        printf("thread_camera: unknown op: %d\n", op);
    }
  } while( ret > 0 && op != OP_END );

  /*
   * Close camera and socket
  */

  printf("thread_camera: exiting\n");

  cap_end();

  if(close(socketfd) == -1)
    perror("thread_camera: close");

  return NULL;
}
