#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/epoll.h>
#include <sys/time.h>

#include "ultrasonic.h"
#include "net.h"

#define ECHO              "/sys/class/gpio/gpio45/value"
#define ECHO_DIRECTION    "/sys/class/gpio/gpio45/direction"
#define ECHO_EDGE         "/sys/class/gpio/gpio45/edge"

#define TRIGGER           "/sys/class/gpio/gpio44/value"
#define TRIGGER_DIRECTION "/sys/class/gpio/gpio44/direction"
#define TRIGGER_EDGE      "/sys/class/gpio/gpio44/edge"

#define DIST_THRESHOLD    20

bool init_gpio() {
  int fd_echo_dir;
  int fd_echo_edge;

  int fd_trig_dir;
  int fd_trig_edge;

  if ((fd_echo_dir = open(ECHO_DIRECTION, O_WRONLY | O_NONBLOCK)) == -1) {
    perror("open");
    return false;
  }
  if ((fd_echo_edge = open(ECHO_EDGE, O_WRONLY | O_NONBLOCK)) == -1) {
    perror("open");
    return false;
  }

  if ((fd_trig_dir = open(TRIGGER_DIRECTION, O_WRONLY)) == -1) {
    perror("open");
    return false;
  }
  if ((fd_trig_edge = open(TRIGGER_EDGE, O_WRONLY)) == -1) {
    perror("open");
    return false;
  }

  /* Make sure pins are initialized */
  if(write(fd_trig_dir,"out",3) == -1) {
    perror("write: fd_trig_dir");
    return false;
  }

  if(write(fd_trig_edge,"none",4) == -1) {
    perror("write: fd_trig_edge");
    return false;
  }

  if(write(fd_echo_dir,"in",2) == -1) {
    perror("write: fd_trig_edge");
    return false;
  }

  if(write(fd_echo_edge,"both",4) == -1) {
    perror("write: fd_trig_edge");
    return false;
  }

  if(close(fd_trig_dir) == -1) {
    perror("close");
    return false;
  }

  if(close(fd_trig_edge) == -1) {
    perror("close");
    return false;
  }

  if(close(fd_echo_dir) == -1) {
    perror("close");
    return false;
  }

  if(close(fd_echo_edge) == -1) {
    perror("close");
    return false;
  }

  return true;
}

void* thread_ultrasonic(void* args) {
  struct th_st_ultrasonic *st = (struct th_st_ultrasonic *)args;

  int port = st->port;
  int socketfd;
  bool in_danger = false;

  int epollfd;
  int fd_echo;
  int fd_trig;
  int ret;
  float dist;
  struct epoll_event ev;
  struct timeval t1;
  struct timeval t2;

  if ((epollfd = epoll_create(1)) == -1) {
    perror("epoll_create");
    return NULL;
  }

  printf("thread_ultrasonic: listening on %d\n", port);

  if((socketfd = open_port(port, "thread_ultrasonic")) == -1)
    return NULL;

  printf("thread_ultrasonic: connected\n");

  if(!init_gpio())
    return NULL;

  if ((fd_echo = open(ECHO, O_RDONLY)) == -1) {
    perror("open:");
    return NULL;
  }
  if ((fd_trig = open(TRIGGER, O_WRONLY)) == -1) {
    perror("open");
    return NULL;
  }

  /* epoll stuff */

  ev.events = EPOLLIN | EPOLLET | EPOLLPRI;
  ev.data.fd = fd_echo;

  if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd_echo, &ev) == -1) {
    perror("epoll_ctl");
    return NULL;
  }

  ret = epoll_wait(epollfd, &ev, 1, -1);

  do {
    if(write(fd_trig, "0", 1) == -1) {
      perror("write: fd_trig");
      return NULL;
    }

    usleep(2);

    if(write(fd_trig, "1", 1) == -1) {
      perror("write: fd_trig");
      return NULL;
    }

    usleep(5);

    if(write(fd_trig, "0", 1) == -1) {
      perror("write: fd_trig");
      return NULL;
    }

    ret = epoll_wait(epollfd, &ev, 1, -1);

    gettimeofday(&t1, NULL);
    ret = epoll_wait(epollfd, &ev, 1, 1000);
    if (ret == -1){
      perror("GPIO: Poll Wait fail");
    }
    else if(ret == 0) {
      printf("TIMEOUT!\n");
    }
    gettimeofday(&t2, NULL);

    dist = ((t2.tv_usec - t1.tv_usec)/2)/29.1f;
    printf("dist %d\n",(int)dist);

    if(in_danger) {
      if((int)dist > DIST_THRESHOLD) {
        printf("send 0\n");
        in_danger = false;
        if(write(socketfd, "0", 1) == -1) {
          perror("write");
          return NULL;
        }

      }
    }
    else {
      if((int)dist <= DIST_THRESHOLD) {
        printf("send 1\n");
        in_danger = true;
        if(write(socketfd, "1", 1) == -1) {
          perror("write");
          return NULL;
        }

      }
    }

    usleep(100000);
  } while(!*st->must_end);

  printf("thread_ultrasonic: exiting\n");

  if(close(socketfd) == -1)
    perror("thread_ultrasonic: close");

  return NULL;
}
