#ifndef __ULTRASONIC_TH__
#define __ULTRASONIC_TH__

#include <stdbool.h>
#include <semaphore.h>

struct th_st_ultrasonic {
  int port;
  bool* must_end;
};

void* thread_ultrasonic(void* args);

#endif
