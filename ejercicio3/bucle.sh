#!/bin/bash

if [ $# -lt 5 ]
then
	echo 'Usage: '$0' times exec stdin stdout stderr'
	exit 1
fi

iters=$1
shift

#echo "$*"
#exit


for i in $(seq 1 $iters)
do
  $1 < $2 > $3 2>> $4
done
