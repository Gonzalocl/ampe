#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <immintrin.h>
#include <iomanip>
#include <stdio.h>
#include <omp.h>
#include <pthread.h>
#include <assert.h>

#define STR1 "Introduzca número de mesas electorales: "
#define STR2 "Introduzca número de partidos políticos: "
#define STR3 "Mostrar datos por pantalla (S/N): "
#define STR4 "Resultados: "
#define STR5 "Total: "
#define STR6 "Tiempo de Cálculo: "
#define STR7 "''."
#define MAX_VOTOS 35000000
#define MAX_VOTOS_MESA 100000

using namespace std;

struct st_hilo {
  int partidos;
  int mesas;
  float* votos;
  float* resultados;
  int tam;
  int indice;
};

void* calcular_partido(void* args) {
  struct st_hilo* s = (struct st_hilo*)args;
  __m256 r1;
  __m256 r2;
  int j;

  for (int i = 0; i < s->mesas; i++) {
    for (j = s->indice; (j + 8) < (s->indice+s->tam); j+= 8) {
      r1 = _mm256_loadu_ps((float *) &s->votos[i*s->partidos + j]);
      r2 = _mm256_loadu_ps((float *) &s->resultados[j]);
      r2 = _mm256_add_ps(r1,r2);

      memcpy(&s->resultados[j], &r2, 8*sizeof(float));
    }
    for(j;j<(s->indice+s->tam);j++)
      s->resultados[j] += s->votos[i*s->partidos + j];
  }

  return NULL;
}

void sumar_AVX_pthread(float* resultados, float* votos, int mesas, int partidos) {
  int n_hilos = omp_get_num_procs();
  pthread_t hilos[n_hilos];
  struct st_hilo st_hilos[n_hilos];

  for (int i = 0; i < n_hilos; i++) {
    st_hilos[i].partidos = partidos;
    st_hilos[i].mesas = mesas;
    st_hilos[i].votos = votos;
    st_hilos[i].resultados = resultados;
    st_hilos[i].tam = partidos/n_hilos;
    st_hilos[i].indice = i * st_hilos[i].tam;

    if(pthread_create(&hilos[i], NULL, calcular_partido, &st_hilos[i]) != 0) {
      perror("pthread_create");
    }
  }

  for (int i = 0; i < n_hilos; i++) {
    if(pthread_join(hilos[i], NULL) != 0) {
      perror("pthread_join");
    }
  }

  for (int i = 0; i < mesas; i++) {
    for(int j=(partidos - (partidos % n_hilos));j<partidos;j++)
      resultados[j] += votos[i*partidos + j];
  }

}

void sumar_AVX(float* resultados, float* votos, int mesas, int partidos) {
  __m256 r1;
  __m256 r2;
  int i;
  int j;

  for (i = 0; i < mesas; i++) {
    for (j = 0; (j + 8) < partidos; j+= 8) {
      r1 = _mm256_loadu_ps((float *) &votos[i*partidos + j]);
      r2 = _mm256_loadu_ps((float *) &resultados[j]);
      r2 = _mm256_add_ps(r1,r2);

      memcpy(&resultados[j], &r2, 8*sizeof(float));
    }
    for(j;j<partidos;j++)
      resultados[j] += votos[i*partidos + j];
  }
}

void sumar(float* resultados, float* votos, int mesas, int partidos) {
  for (int i = 0; i < mesas; i++) {
    for (int j = 0; j < partidos; j++) {
      resultados[j] += votos[i*partidos + j];
    }
  }
}

int main() {
  float* votos;
  float* resultados;
  long mesas;
  long partidos;
  bool mostrar;
  char SN;
  struct timeval t1;
  struct timeval t2;

  cout << STR1;
  cin >> mesas;
  cout << STR2;
  cin >> partidos;
  cout << STR3;
  cin >> SN;

  mostrar = (SN == 'S');
  size_t size1 = mesas*partidos*sizeof(int);
  size_t size2 = partidos*sizeof(int);

  if((votos = (float*)malloc(size1)) == NULL) {
    perror("malloc");
    return EXIT_FAILURE;
  }

  int restantes = MAX_VOTOS;
  srand(time(NULL));
  int m = 0;
  int p = 0;
  while (m < mesas && restantes > 0) {
    p = 0;
    while (p < partidos && restantes > 0) {
      votos[m*partidos + p] = (float) (rand() % ( MAX_VOTOS_MESA < restantes ? MAX_VOTOS_MESA : restantes ));
      restantes -= votos[m*partidos + p];
      p++;
    }
    m++;
  }

  if((resultados = (float*)malloc(size2)) == NULL) {
    perror("malloc");
    return EXIT_FAILURE;
  }

  memset(resultados, 0, size2);

  #ifdef Vsec
  cout << "Vsec" << endl;
  gettimeofday(&t1, NULL);
  sumar(resultados,  votos, mesas, partidos);
  #elif defined Vavx
  cout << "Vavx" << endl;
  gettimeofday(&t1, NULL);
  sumar_AVX(resultados,  votos, mesas, partidos);
  #elif defined Vpth
  cout << "Vpth" << endl;
  gettimeofday(&t1, NULL);
  sumar_AVX_pthread(resultados,  votos, mesas, partidos);
  #endif
  gettimeofday(&t2, NULL);

  float tiempo = ((double)t2.tv_sec + (double)t2.tv_usec/1000000) -
                 ((double)t1.tv_sec + (double)t1.tv_usec/1000000);


  if (mostrar) {
    cout << STR4 << endl;
    for (int i = 0; i < mesas; i++) {
      cout << setw(18) << (int)votos[i*partidos];
      for (int j = 1; j < partidos; j++) {
        cout << setw(10) << (int)votos[i*partidos + j];
      }
      cout << endl;
    }

    cout << STR5 << setw(18-strlen(STR5)) << (int)resultados[0];
    for (int i = 1; i < partidos; i++) {
      cout << setw(10) << (int)resultados[i];
    }
    cout << endl;
  }

  cout << STR6;
  cerr << fixed << setprecision(4) << tiempo;
  cout << STR7;
  cerr << endl;
}
