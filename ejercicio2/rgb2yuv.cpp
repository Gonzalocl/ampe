#include <iostream>
#include <string.h>
#include <sys/time.h>
#include <immintrin.h>
#include <unistd.h>
#include <omp.h>
#include <stdio.h>


#define RED 0
#define GREEN 1
#define BLUE 2
#define Y 0
#define U 1
#define V 2

#define YR 0.299f
#define YG 0.587f
#define YB 0.114f

#define UR -0.147f
#define UG -0.289f
#define UB 0.436f

#define VR 0.615f
#define VG -0.515f
#define VB -0.1f



using namespace std;


void rgb2yuv(int n, float* src, float* dst);
void rgb2yuv_vec(int n, float* src, float* dst);
void rgb2yuv_vec_omp(int n, float* src, float* dst);
void rgb2yuv_vec2(int n, float* src, float* dst);
void rgb2yuv_vec3(int n, float* src, float* dst);

float min(float a, float b) {
  return (a < b) ? a : b;
}

float max(float a, float b) {
  return (a > b) ? a : b;
}

int main(int argc, char* argv[]) {

  struct timeval t1;
  struct timeval t2;

  int n;
  cin >> n;
  float* src = (float*)malloc(n*n*3*sizeof(float));
  float* dst = (float*)malloc(n*n*3*sizeof(float));

  if(src == NULL || dst == NULL) {
    perror("malloc");
    return EXIT_FAILURE;
  }

  for (long i = 0; i < n*n*3; i++) {
    cin >> src[i];
  }

  #ifdef V1
  //cerr << "v1" << endl;
  gettimeofday(&t1, NULL);
  rgb2yuv(n, src, dst);
  #elif defined V2
  //cerr << "v2" << endl;
  gettimeofday(&t1, NULL);
  rgb2yuv_vec(n, src, dst);
  #elif defined Vomp
  //cerr << "vomp" << endl;
  gettimeofday(&t1, NULL);
  rgb2yuv_vec_omp(n, src, dst);
  #elif defined V3
  //cerr << "v3" << endl;
  gettimeofday(&t1, NULL);
  rgb2yuv_vec2(n, src, dst);
  #elif defined V4
  //cerr << "v4" << endl;
  gettimeofday(&t1, NULL);
  rgb2yuv_vec3(n, src, dst);
  #endif
  gettimeofday(&t2, NULL);
  float tiempo = ((double)t2.tv_sec + (double)t2.tv_usec/1000000) -
                 ((double)t1.tv_sec + (double)t1.tv_usec/1000000);
  cerr << tiempo << endl;

  char* buf = (char*)malloc(n*n*3*4);
  long count = 0;

  if(buf == NULL) {
    perror("malloc");
    return EXIT_FAILURE;
  }

  for (long i = 0; i < n*n*3; i++) {
    count += snprintf(buf + count, 5, "%d\n", (int)dst[i]);
  }
  write(STDOUT_FILENO, buf, count);

  free(buf);
  free(src);
  free(dst);
}

void rgb2yuv(int n, float* src, float* dst) {
  for (long i = 0; i < n*n*3; i+=3) {
      dst[i+Y] = src[i+RED]*YR + src[i+GREEN]*YG + src[i+BLUE]*YB;
      dst[i+U] = src[i+RED]*UR + src[i+GREEN]*UG + src[i+BLUE]*UB + 128.0f;
      dst[i+V] = src[i+RED]*VR + src[i+GREEN]*VG + src[i+BLUE]*VB + 128.0f;
      if(dst[i+V] < 0) dst[i+V] = 0;
      else if (dst[i+V] > 255) dst[i+V] = 255;
  }
}

void rgb2yuv_vec_omp(int n, float* src, float* dst) {
  __m256 c = _mm256_set_ps(0, 128.0f, 128.0f, 0, 0, 0, 0, 0);
  __m256 const_r = _mm256_set_ps(YR, UR, VR, 0, 0, 0, 0, 0);
  __m256 const_g = _mm256_set_ps(YG, UG, VG, 0, 0, 0, 0, 0);
  __m256 const_b = _mm256_set_ps(YB, UB, VB, 0, 0, 0, 0, 0);
  omp_set_num_threads(8);
  #pragma omp parallel for
  for (long i = 0; i < n*n*3; i+=3) {
      __m256 r = _mm256_set1_ps(src[i+RED]);
      __m256 g = _mm256_set1_ps(src[i+GREEN]);
      __m256 b = _mm256_set1_ps(src[i+BLUE]);

      __m256 a1 = _mm256_mul_ps(r, const_r);
      __m256 a2 = _mm256_mul_ps(g, const_g);
      __m256 a3 = _mm256_fmadd_ps(b, const_b, c);

      __m256 a4 = _mm256_add_ps(a1, a2);
      __m256 a5 = _mm256_add_ps(a4, a3);

      dst[i+V] = min(255,max(0,a5[7-V]));
      dst[i+Y] = a5[7-Y];
      dst[i+U] = a5[7-U];

  }
}

void rgb2yuv_vec(int n, float* src, float* dst) {
  __m256 c = _mm256_set_ps(0, 128.0f, 128.0f, 0, 0, 0, 0, 0);
  __m256 const_r = _mm256_set_ps(YR, UR, VR, 0, 0, 0, 0, 0);
  __m256 const_g = _mm256_set_ps(YG, UG, VG, 0, 0, 0, 0, 0);
  __m256 const_b = _mm256_set_ps(YB, UB, VB, 0, 0, 0, 0, 0);
  for (long i = 0; i < n*n*3; i+=3) {
      __m256 r = _mm256_set1_ps(src[i+RED]);
      __m256 g = _mm256_set1_ps(src[i+GREEN]);
      __m256 b = _mm256_set1_ps(src[i+BLUE]);

      __m256 a1 = _mm256_mul_ps(r, const_r);
      __m256 a2 = _mm256_mul_ps(g, const_g);
      __m256 a3 = _mm256_fmadd_ps(b, const_b, c);

      __m256 a4 = _mm256_add_ps(a1, a2);
      __m256 a5 = _mm256_add_ps(a4, a3);

      dst[i+V] = min(255,max(0,a5[7-V]));
      dst[i+Y] = a5[7-Y];
      dst[i+U] = a5[7-U];
  }
}

void rgb2yuv_vec2(int n, float* src, float* dst) {
  __m256 c = _mm256_set_ps(0, 128.0f, 128.0f, 0, 128.0f, 128.0f, 0, 0);
  __m256 const_r = _mm256_set_ps(YR, UR, VR, YR, UR, VR, 0, 0);
  __m256 const_g = _mm256_set_ps(YG, UG, VG, YG, UG, VG, 0, 0);
  __m256 const_b = _mm256_set_ps(YB, UB, VB, YB, UB, VB, 0, 0);
  for (long i = 0; i < (n*n*3)-3; i+=6) {
      __m256 r = _mm256_set_ps(src[i+RED],   src[i+RED],   src[i+RED],   src[3+i+RED],   src[3+i+RED],   src[3+i+RED],   0, 0);
      __m256 g = _mm256_set_ps(src[i+GREEN], src[i+GREEN], src[i+GREEN], src[3+i+GREEN], src[3+i+GREEN], src[3+i+GREEN], 0, 0);
      __m256 b = _mm256_set_ps(src[i+BLUE],  src[i+BLUE],  src[i+BLUE],  src[3+i+BLUE],  src[3+i+BLUE],  src[3+i+BLUE],  0, 0);

      __m256 a1 = _mm256_mul_ps(r, const_r);
      __m256 a2 = _mm256_mul_ps(g, const_g);
      __m256 a3 = _mm256_fmadd_ps(b, const_b, c);

      __m256 a4 = _mm256_add_ps(a1, a2);
      __m256 a5 = _mm256_add_ps(a4, a3);

      dst[i+V] = min(255,max(0,a5[7-V]));
      dst[i+V+3] = min(255,max(0,a5[7-V-3]));

      dst[i+Y] = a5[7-Y];
      dst[i+U] = a5[7-U];

      dst[i+Y+3] = a5[7-Y-3];
      dst[i+U+3] = a5[7-U-3];

  }
  if ((n*n)%2 == 1) {
      int i = n*n*3-3;
      __m256 r = _mm256_set1_ps(src[i+RED]);
      __m256 g = _mm256_set1_ps(src[i+GREEN]);
      __m256 b = _mm256_set1_ps(src[i+BLUE]);

      __m256 a1 = _mm256_mul_ps(r, const_r);
      __m256 a2 = _mm256_mul_ps(g, const_g);
      __m256 a3 = _mm256_fmadd_ps(b, const_b, c);

      __m256 a4 = _mm256_add_ps(a1, a2);
      __m256 a5 = _mm256_add_ps(a4, a3);

      dst[i+V] = min(255,max(0,a5[7-V]));
      dst[i+Y] = a5[7-Y];
      dst[i+U] = a5[7-U];

  }


}


void rgb2yuv_vec3(int n, float* src, float* dst) {
  __m256 c1 = _mm256_set_ps(     0, 128.0f, 128.0f,      0, 128.0f, 128.0f,      0, 128.0f);
  __m256 c2 = _mm256_set_ps(128.0f,      0, 128.0f, 128.0f,      0, 128.0f, 128.0f,      0);
  __m256 c3 = _mm256_set_ps(128.0f, 128.0f,      0, 128.0f, 128.0f,      0, 128.0f, 128.0f);

  __m256 const_r1 = _mm256_set_ps(YR, UR, VR, YR, UR, VR, YR, UR);
  __m256 const_r2 = _mm256_set_ps(VR, YR, UR, VR, YR, UR, VR, YR);
  __m256 const_r3 = _mm256_set_ps(UR, VR, YR, UR, VR, YR, UR, VR);

  __m256 const_g1 = _mm256_set_ps(YG, UG, VG, YG, UG, VG, YG, UG);
  __m256 const_g2 = _mm256_set_ps(VG, YG, UG, VG, YG, UG, VG, YG);
  __m256 const_g3 = _mm256_set_ps(UG, VG, YG, UG, VG, YG, UG, VG);


  __m256 const_b1 = _mm256_set_ps(YB, UB, VB, YB, UB, VB, YB, UB);
  __m256 const_b2 = _mm256_set_ps(VB, YB, UB, VB, YB, UB, VB, YB);
  __m256 const_b3 = _mm256_set_ps(UB, VB, YB, UB, VB, YB, UB, VB);

  for (long i = 0; i < (n*n*3)-3*(8-1); i+=3*8) {
      __m256 r1 = _mm256_set_ps(src[i+RED],      src[i+RED],      src[i+RED],      src[3+i+RED],    src[3+i+RED],    src[3+i+RED],    src[6+i+RED],    src[6+i+RED]);
      __m256 r2 = _mm256_set_ps(src[6+i+RED],    src[9+i+RED],    src[9+i+RED],    src[9+i+RED],    src[12+i+RED],   src[12+i+RED],   src[12+i+RED],   src[15+i+RED]);
      __m256 r3 = _mm256_set_ps(src[15+i+RED],   src[15+i+RED],   src[18+i+RED],   src[18+i+RED],   src[18+i+RED],   src[21+i+RED],   src[21+i+RED],   src[21+i+RED]);

      __m256 g1 = _mm256_set_ps(src[i+GREEN],    src[i+GREEN],    src[i+GREEN],    src[3+i+GREEN],  src[3+i+GREEN],  src[3+i+GREEN],  src[6+i+GREEN],  src[6+i+GREEN]);
      __m256 g2 = _mm256_set_ps(src[6+i+GREEN],  src[9+i+GREEN],  src[9+i+GREEN],  src[9+i+GREEN],  src[12+i+GREEN], src[12+i+GREEN], src[12+i+GREEN], src[15+i+GREEN]);
      __m256 g3 = _mm256_set_ps(src[15+i+GREEN], src[15+i+GREEN], src[18+i+GREEN], src[18+i+GREEN], src[18+i+GREEN], src[21+i+GREEN], src[21+i+GREEN], src[21+i+GREEN]);

      __m256 b1 = _mm256_set_ps(src[i+BLUE],     src[i+BLUE],     src[i+BLUE],     src[3+i+BLUE],   src[3+i+BLUE],   src[3+i+BLUE],   src[6+i+BLUE],   src[6+i+BLUE]);
      __m256 b2 = _mm256_set_ps(src[6+i+BLUE],   src[9+i+BLUE],   src[9+i+BLUE],   src[9+i+BLUE],   src[12+i+BLUE],  src[12+i+BLUE],  src[12+i+BLUE],  src[15+i+BLUE]);
      __m256 b3 = _mm256_set_ps(src[15+i+BLUE],  src[15+i+BLUE],  src[18+i+BLUE],  src[18+i+BLUE],  src[18+i+BLUE],  src[21+i+BLUE],  src[21+i+BLUE],  src[21+i+BLUE]);


      __m256 a1_1 = _mm256_mul_ps(r1, const_r1);
      __m256 a1_2 = _mm256_mul_ps(r2, const_r2);
      __m256 a1_3 = _mm256_mul_ps(r3, const_r3);



      __m256 a2_1 = _mm256_mul_ps(g1, const_g1);
      __m256 a2_2 = _mm256_mul_ps(g2, const_g2);
      __m256 a2_3 = _mm256_mul_ps(g3, const_g3);


      __m256 a3_1 = _mm256_fmadd_ps(b1, const_b1, c1);
      __m256 a3_2 = _mm256_fmadd_ps(b2, const_b2, c2);
      __m256 a3_3 = _mm256_fmadd_ps(b3, const_b3, c3);

      __m256 a4_1 = _mm256_add_ps(a1_1, a2_1);
      __m256 a4_2 = _mm256_add_ps(a1_2, a2_2);
      __m256 a4_3 = _mm256_add_ps(a1_3, a2_3);

      __m256 a5_1 = _mm256_add_ps(a4_1, a3_1);
      __m256 a5_2 = _mm256_add_ps(a4_2, a3_2);
      __m256 a5_3 = _mm256_add_ps(a4_3, a3_3);

      dst[i+V] = min(255,max(0,a5_1[7-V]));
      dst[i+V+3] = min(255,max(0,a5_1[7-V-3]));

      dst[i+V+6] = min(255,max(0,a5_2[7]));
      dst[i+V+9] = min(255,max(0,a5_2[7-3]));
      dst[i+V+12] = min(255,max(0,a5_2[7-6]));

      dst[i+V+15] = min(255,max(0,a5_3[7-1]));
      dst[i+V+18] = min(255,max(0,a5_3[7-4]));
      dst[i+V+21] = min(255,max(0,a5_3[7-7]));

      dst[i+Y] = a5_1[7-0];
      dst[i+U] = a5_1[7-1];

      dst[i+Y+3] = a5_1[7-3];
      dst[i+U+3] = a5_1[7-4];

      dst[i+Y+6] = a5_1[7-6];
      dst[i+U+6] = a5_1[7-7];

      dst[i+Y+9] = a5_2[7-1];
      dst[i+U+9] = a5_2[7-2];

      dst[i+Y+12] = a5_2[7-4];
      dst[i+U+12] = a5_2[7-5];

      dst[i+Y+15] = a5_2[7-7];
      dst[i+U+15] = a5_3[7-0];

      dst[i+Y+18] = a5_3[7-2];
      dst[i+U+18] = a5_3[7-3];

      dst[i+Y+21] = a5_3[7-5];
      dst[i+U+21] = a5_3[7-6];

  }

}

