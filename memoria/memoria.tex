\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{array}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{standalone} %para poder incluir el grammar.tex
\usepackage[hidelinks]{hyperref}
\usepackage{svg}
\usepackage[final]{listings}
\usepackage{xcolor} %para los colores de C
\usepackage{color} 
\usepackage{fontspec}
\usepackage{xcolor,colortbl}
\usepackage{array,booktabs,arydshln,xcolor}
\newcommand\VRule[1][\arrayrulewidth]{\vrule width #1} %para lineas gruesas
\usepackage{float}
\usepackage[margin=1.25in]{geometry}
\hypersetup{
colorlinks=true,
linkcolor=black,
citecolor  = black,
urlcolor=blue
}

\newcommand*\newpar{\par \vspace{4mm}}
\pagestyle{fancy}
\setlength{\headheight}{15pt} 
\selectlanguage{spanish}

\usepackage{tikz}
\usepackage{pgfplots}

\usetikzlibrary{patterns}

%Cabecera
\lhead{}
\chead{}
\rhead{\bfseries Arquitecturas Multimedia y de Propósito Específico}

\begin{document}

\begin{titlepage}
	\centering
	\includegraphics[scale=0.4]{um}
	\\
	\vspace{0.5cm}
	\Large Universidad de Murcia
	\\
	\large Facultad de Informática
	\vspace{1cm}
	\\
	\includegraphics[scale=0.7]{fium}
	\\
	\vspace{0.25cm}
	\noindent\rule{\textwidth}{0.4mm}
	{\Huge\bfseries Programación de Extensiones Multimedia(SSE/AVX)\par}
	{\Large Arquitecturas Multimedia y de Propósito Específico\par}
	\noindent\rule{\textwidth}{0.4mm}	
	\vspace{0.5cm}
	\\
	Pablo Martínez Sánchez 
	\\
	\texttt{pabloantonio.martinezs@um.es}
	\\
	Gonzalo Caparrós Laiz
	\\
	\texttt{gonzalo.caparrosl@um.es}
	\\
	\vfill
	4º GII Mención
	\\
	Ingeniería de Computadores
	\vfill
	\vspace{0.15cm}
	{\large \today\par}
\end{titlepage}

\definecolor{backgroundColour}{rgb}{0.95,0.95,0.93}
\newfontfamily{\lstmonaco}[Scale=0.85]{Monaco}
\newfontfamily{\lstlittlemonaco}[Scale=0.6]{Monaco}

\lstset{language=C++,
  backgroundcolor=\color{backgroundColour},
  basicstyle=\ttfamily,
  keywordstyle=\color{blue}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  commentstyle=\color{green}\ttfamily,
  morecomment=[l][\color{magenta}]{\#}
}

\lstdefinestyle{term}{
  backgroundcolor=\color{backgroundColour},
  numbers=none,
  basicstyle=\lstmonaco,
}

\lstdefinestyle{asm}{
  backgroundcolor=\color{backgroundColour},
  basicstyle=\lstlittlemonaco,
}


\newpage
\tableofcontents
\newpage

\section{Introducción}

Para evaluar los tiempos de ejecución, usaremos dos máquinas distintas, cuyo hardware
y software se ven resumidos en la siguiente tabla:

\begin{table}[H]
\begin{tabular}{ | l | c | c | c | l | }
\hline
Nombre CPU          & Frecuencia & Cores & LLC  & OS            \\ \hline \hline
\href{https://ark.intel.com/es-es/products/88185/Intel-Core-i5-6400-Processor-6M-Cache-up-to-3_30-GHz}{Intel\textsuperscript{\textregistered} Core\textsuperscript{TM} i5-6400}   & 3.3 GHz    & 4     & 6 MB  & Linux 4.2.0   \\ \hline
\href{https://ark.intel.com/es-es/products/80807/Intel-Core-i7-4790K-Processor-8M-Cache-up-to-4_40-GHz}{Intel\textsuperscript{\textregistered} Core\textsuperscript{TM} i7-4790K} & 4.4 GHz    & 4(HT) & 8 MB  & Linux 4.18.14 \\ \hline
\end{tabular}
\centering
\caption{Resumen de las máquinas donde se llevarán a cabo las pruebas}
\end{table}

Las dos máquinas incorporan el conjunto de instrucciones multimedia 
AVX(\textit{advanced vector extensions}) y AVX2, así como FMA3(\textit{fused
multiply add}), que se usará en varios ejercicios. Esta instrucción realiza un producto
y una suma en una misma instrucción, con una latencia y CPI equivalente al de una
suma o producto(compárese \texttt{\_mm256\_mul\_ps}~\cite{mulps} y 
\texttt{\_mm256\_fmadd\_ps}~\cite{fmaddps}).\newpar

En cuanto a los compiladores, usaremos el compilador de Intel, el 
\texttt{icc}/\texttt{icpc} en su versión \texttt{16.0.0} y el compilador
 \texttt{gcc} en su versión \texttt{8.2.1}. \newpar
 
En la medición de todos los tiempos, el tiempo que se muestra es el resultado de la media
ponderada de 10 ejecuciones. La medición se realiza sólo sobre la parte de cómputo, ignorando
así la entrada/salida. \newpar

Para la compilación usaremos siempre el comando \texttt{make}, con su correspondiente \texttt{Makefile}
donde pueden consultarse todas las opciones de compilación.

\newpage

\section{Transformada de Wavelet}

\subsection{Breve manual de usuario}
Para compilar, usamos el comando \texttt{make}, lo que nos generará los ejecutables.
Podemos indicar el tamaño del vector mediante la opción \texttt{--n=tam}, o ver la ayuda
con \texttt{--help}. Si no se indica el tamaño del vector, el valor por defecto es de 1000000.

\begin{lstlisting}[style=term]
[pablo@ampe ejercicio1]$ ./wtgcc 
0.0028
[pablo@ampe ejercicio1]$ ./wtgcc --n=100000000
0.3167
\end{lstlisting}

\subsection{Listado de ficheros fuente}

\begin{itemize}
\item wt.c
\item wt\_clases\_sse.c
\item wt\_fma.c
\item wt\_sse.c
\item Makefile
\end{itemize}

\subsection{Implementación}
Si comparamos el código de \texttt{wt.c} y \texttt{wt\_sse.c}, vemos que el primero
lleva a cabo el cálculo de la transformada en la función \texttt{wt\_4d()} de forma secuencial
y sin vectorización. Por otro lado, en el fichero \texttt{wt\_sse.c} vemos que se llama a
\texttt{wt\_4d\_SSE()}, donde se utilizan \textit{intrinsics} para preparar los datos y
se llama a la función \texttt{calcular\_coeficientes\_all()}, que lleva a cabo el cálculo de la
transformada en ensamblador, usando las extensiones multimedia SSE. \newpar

Puesto que el tipo de datos que se usa es \texttt{float}(32 bits), y que la longitud del
vector de las extensiones SSE es de 128 bits, podemos almacenar 4 \texttt{floats} en un
registro, y por lo tanto esperamos conseguir un \textit{speedup} máximo(ideal) de 4x.

\subsection{Benchmarks}
Para este ejercicio, llevamos a cabo varias versiones y probamos su rendimiento, con una entrada
de $n=100 000 000$. Estas versiones pueden consultarse en el \texttt{Makefile}, 
a continuación se hace un pequeño resumen para entender los resultados posteriores:

\begin{itemize}
\item \textbf{gcc}: Fichero \texttt{wt.c} compilado con \texttt{gcc -O2}.

\item \textbf{icc}: Fichero \texttt{wt.c} compilado con \texttt{icc -O2 -no-vec}, con la
intención de comparar ambos compiladores y forzando que el \texttt{icc} no vectorice.

\item \textbf{AVX}: Fichero \texttt{wt.c} compilado con \texttt{icc -O2 -xCORE-AVX2},
activando la vectorización automática del \texttt{icc}, indicando que deseamos utilizar
las extensiones multimedia AVX.

\item \textbf{SSE2}: Fichero \texttt{wt\_sse.c} compilado con \texttt{icc -O2 -xSSE2}

\item \textbf{SSE2-c}: Fichero \texttt{wt\_sse\_clases.c} compilado con \texttt{icpc -O2 -xSSE2},
donde tratamos de comparar una implementación en SSE en ensamblador(la anterior) contra
una hecha mediante la librería de clases \texttt{fvec}.

\item \textbf{FMA-c}: Fichero \texttt{wt\_fma.c} compilado con \texttt{icpc -O2 -xSSE2}, añadiendo
a la versión anterior instrucciones \texttt{FMA}. Puesto que estamos usando las clases \texttt{fvec},
no podemos utilizar explícitamente esta instrucción, pero sí intentamos que el compilador la use,
realizando una operación de la forma $a*b + c$ que debería facilitar el trabajo al compilador.

\end{itemize}

\begin{figure}[H]
\centering

\begin{tikzpicture}

\tikzstyle{every node}=[font=\small]

\begin{axis}[
  %scale only axis,
  height=7cm,
  width=11cm,
  ymax=0.4,
  ymin=0,
  ybar, %bar chart
  symbolic x coords={gcc,icc,AVX,SSE2,SSE2-c,FMA-c},
  ylabel={Tiempo(s)},
]

\addplot+[area legend, black,fill=blue!40, postaction={pattern=north east lines}] table [x=Version,y=i7-4790K]  {../ejercicio1/salidas/tiempos.csv};
\addplot+[area legend, black,fill=red!40,  postaction={pattern=north east lines}] table [x=Version,y=i5-6400]  {../ejercicio1/salidas/tiempos.csv};
\legend{i7-4790K, i5-6400}

\end{axis}

\end{tikzpicture}

\caption{Resumen de las distintas versiones para distintos Intel Core, tiempos en el fichero \textit{tiempos.csv}.}
\end{figure}

Vemos que el \texttt{gcc} obtiene tiempos ligeramente peores que el \texttt{icc}, 
por ello, \texttt{icc} es el compilador utilizado para el resto de versiones de este ejercicio. 
El \texttt{icc} consigue unos resultados ligeramente mejores con vectorización automática 
para AVX que el código ensamblador que usa SSE. Por otro lado, el tiempo entre la versión
AVX, SSE con clases y la versión de FMA son prácticamente idénticos. La carga de trabajo
es demasiado pequeña para sacar más conclusiones, y no podemos aumentarla porque no tenemos
suficiente memoria física(RAM) para hacerlo. En el i7-4790K conseguimos un \texttt{speedup} de
$0.3089/0.2354=1.31x$.

\newpage

\section{Transformar una matriz NxN del formato RGB a YUV}

\subsection{Breve manual de usuario}
Para compilar, usamos el comando \texttt{make}, lo que nos generará los ejecutables.
Este ejecutable no tiene argumentos, se limita a leer de la entrada estándar la imagen y
cuando la haya leido, a escribir por la salida estándar la imagen transformada. La imagen 
de entrada debe contener una primera línea con el número de píxeles de la imagen(la imagen debe
ser cuadrada). El resto de líneas representan los píxeles, que deben representarse en formato ASCII.
Podemos introducir la entrada y sacar la salida mediante ficheros gracias a la redirección, por
medio del intérprete de órdenes:

\begin{lstlisting}[style=term]
[pablo@ampe ejercicio2]$ ./rgb2yuv2 < mona.ppm > mona-yuv.ppm
0.0838501
\end{lstlisting}

\subsection{Listado de ficheros fuente}

\begin{itemize}
\item rgb2yuv.cpp
\item Makefile
\end{itemize}

\label{impej2}
\subsection{Implementación}
El código para llevar a cabo la transformada se encuentra en el fichero \texttt{rgb2yuv.cpp},
donde hay implementadas varias versiones, que pueden ser seleccionadas definiendo los valores
apropiados en tiempo de compilación(con \texttt{-DV1}, por ejemplo, definimos \texttt{V1}).
Así, si se ha definido un determinado valor, la función que se compila es una u otra.
Dichas versiones son:

\begin{enumerate}
\item \texttt{V1 (rgb2yuv)}: Versión secuencial y sin vectorizar.

\item \texttt{V2 (rgb2yuv\_vec)}: Versión vectorizada con \textit{intrinsics} AVX
aprovechando 3 de las 8 posibles entradas del vector(una entrada para Y,U y V, respectivamente).

\item \texttt{Vomp (rgb2yuv\_vec\_omp)}: Igual que \texttt{V2}, añadiendo paralelismo con \texttt{OpenMP}.

\item \texttt{V3 (rgb2yuv\_vec2)}: Versión vectorizada aprovechando 6 de las 8 posibles entradas.

\item \texttt{V4 (rgb2yuv\_vec3)}: Versión vectorizada aprovechando todo el vector.
\end{enumerate}

Para leer la imagen en formato RGB, primero leemos el tamaño de dicha imagen, en píxeles.
Dado que una restricción es que la imagen debe ser cuadrada, ya tenemos el tamaño de la imagen.
Después de esto, se leen los píxeles de la imagen, en formato RGB(en ASCII) en un bucle
de $n*n*3$, ya que un píxel se compone de 3 bytes:

\begin{lstlisting}
int main(int argc, char* argv[]) {
  ...
  
  int n;
  cin >> n;
  float* src = (float*)malloc(n*n*3*sizeof(float));
  float* dst = (float*)malloc(n*n*3*sizeof(float));

  for (long i = 0; i < n*n*3; i++) {
    cin >> src[i];
  }
}
\end{lstlisting}

Tenemos además varios \texttt{define},
donde definimos la distancia de cada byte respecto al primero($Y=0$, $U=1$, $V=2$) y los
valores que permiten convertir de un formato a otro mediante un producto.

\begin{lstlisting}
#define Y 0
#define U 1
#define V 2

#define YR  0.299f
#define YG  0.587f
#define YB  0.114f

#define UR -0.147f
#define UG -0.289f
#define UB  0.436f

#define VR  0.615f
#define VG -0.515f
#define VB -0.1f
\end{lstlisting}

Si vemos el código básico, sin vectorizar, que lleva cabo la transformación:

\begin{lstlisting}[
  captionpos=b,
  caption=Versión secuencial sin vectorizar]
  
void rgb2yuv(int n, float* src, float* dst) {
  for (long i = 0; i < n*n*3; i+=3) {
   dst[i+Y]=src[i+RED]*YR+src[i+GREEN]*YG+src[i+BLUE]*YB;
   dst[i+U]=src[i+RED]*UR+src[i+GREEN]*UG+src[i+BLUE]*UB + 
                                                  128.0f;
   dst[i+V]=src[i+RED]*VR+src[i+GREEN]*VG+src[i+BLUE]*VB + 
                                                  128.0f;
   if(dst[i+V] < 0) dst[i+V] = 0;
   else if (dst[i+V] > 255) dst[i+V] = 255;
  }
}
\end{lstlisting}

Almacenamos los píxeles en formato RGB en \texttt{src} y el resultado, en YUV,
en \texttt{dst}(ambos del mismo tamaño, $n*n*3$) En cada iteración del bucle convertimos
un píxel(es decir, 3 bytes) aplicando la transformación:

\begin{equation*}
\begin{aligned}
Y \ &= \ 0.299*R  \  &+ \ 0.587*G \ &+ \ 0.144*B \\
U \ &= \ -0.147*R \ &-  \ 0.289*G \ &+ \ 0.436*B \\
V \ &= \ 0.615*R  \  &- \ 0.515*G \ &+ \ 0.100*B \\
\end{aligned}
\end{equation*}

Si ahora vemos el código vectorizado:

\begin{lstlisting}[
  captionpos=b,
  caption=Versión vectorizada]  
void rgb2yuv_vec(int n, float* src, float* dst) {
  __m256 c = _mm256_set_ps(0,128.0f,128.0f,0,0,0,0,0);
  __m256 const_r = _mm256_set_ps(YR,UR,VR,0,0,0,0,0);
  __m256 const_g = _mm256_set_ps(YG,UG,VG,0,0,0,0,0);
  __m256 const_b = _mm256_set_ps(YB,UB,VB,0,0,0,0,0);
  for (long i = 0; i < n*n*3; i+=3) {
      __m256 r = _mm256_set1_ps(src[i+RED]);
      __m256 g = _mm256_set1_ps(src[i+GREEN]);
      __m256 b = _mm256_set1_ps(src[i+BLUE]);

      __m256 a1 = _mm256_mul_ps(r, const_r);
      __m256 a2 = _mm256_mul_ps(g, const_g);
      __m256 a3 = _mm256_fmadd_ps(b, const_b, c);

      __m256 a4 = _mm256_add_ps(a1, a2);
      __m256 a5 = _mm256_add_ps(a4, a3);

      dst[i+V] = min(255,max(0,a5[7-V]));
      dst[i+Y] = a5[7-Y];
      dst[i+U] = a5[7-U];
  }
}
\end{lstlisting}

En esta versión solo aprovechamos 3 de los 8 \texttt{float}s del vector. Fuera del bucle
rellenamos los registros con las constantes necesarias. Dentro del bucle, lo primero que
se hace es rellenar los registros \texttt{r}, \texttt{g} y \textbf{b} con el valor del byte del píxel
que queremos transformar(\texttt{set1\_ps} establece el mismo valor en todas las posiciones
del registro) \newpar

En \texttt{a1} almacenamos el cálculo de la primera parte de la $Y$, $U$ y $V$, en
\texttt{a2} la segunda parte y en \texttt{a3} la tercera parte(esto se refleja en
las siguiente figura). Posteriormente sumamos los tres vectores y obtenemos el 
cálculo final en \texttt{a5}. 

\usetikzlibrary{babel, shapes, arrows, shadows, positioning, backgrounds, fit}
\usetikzlibrary{patterns}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}

\begin{figure}[H]
\centering

\begin{tikzpicture}     
\node [
       draw,
       scale=1,
       rectangle split, 
       rectangle split horizontal, 
       rectangle split parts=3,
       rectangle split part fill={orange!40,orange!40,orange!40}
      ] (s0) 
      { 
       100 
       \nodepart{two}  200
       \nodepart{three} 150
      };    

\node [
       draw,
       below=0.5cm of s0,
       scale=1,
       rectangle split, 
       rectangle split horizontal, 
       rectangle split parts=3,
       rectangle split part fill={blue!40,blue!40,blue!40}
      ] (s1) 
      { 
       100 
       \nodepart{two}  100
       \nodepart{three}100
      }; 
      
\node [
       draw,
       below=0.5cm of s1,
       scale=1,
       rectangle split, 
       rectangle split horizontal, 
       rectangle split parts=3,
       rectangle split part fill={red!40,red!40,red!40}
      ] (s2) 
      { 
       0.299 
       \nodepart{two}  -0.147
       \nodepart{three} 0.615
      };                           
        
\draw [dashed,line width=0.3mm] ($ (s0.east) + (0.2,0) $) --  ($ (s0.east) + (1.5,0) $);
\draw [dashed,line width=0.3mm] ($ (s1.east) + (0.2,0) $) --  ($ (s1.east) + (1.5,0) $);

\node [left=-0.001cm of s0.west] {$imagen$};
\node [left=-0.001cm of s1.west] {$r$};
\node [left=-0.001cm of s2.west] {$const\_r$};

\node[text width=6cm] at ($ (s0.north) + (2,0.25) $) 
{
R \ \ \ G \ \ \ B
};

\node[text width=5cm] at ($ (s2.east) + (-1.5,-2.25) $) (text)
{ 
\large Y \ = \ 0.299  \ \ * R \ + \ ... \\
\large U \ = \ -0.147 \ * R \ \ - \ ... \\
\large V \ = \ 0.615  \ \ * R \ + \ ...
};

\draw [thick, decorate, decoration={brace, amplitude=4}]
      ($ (s2.east) + (0,-0.5) $) --
      ($ (s2.west) + (0,-0.5) $);
\draw[thick, -stealth]
      ($ (s2.north) + (0,-0.9) $) --
      ($ (s2.north) +  (0,-1.5) $);  
      
\draw [thick, -stealth] (s1.east) -- ($ (s1.east) + (1,0) $) -- ($ (s1.east) + (1,-1.25) $) -- ($ (text.north) + (1,0.1) $);

%borde
\draw [draw=red, fill=none]  ($ (text.west) + (1.25,1) $) rectangle ($ (text.east) + (-2.5,-1) $);
\draw [draw=blue, fill=none] ($ (text.west) + (3.25,1) $) rectangle ($ (text.east) + (-1.25,-1) $);
        
\end{tikzpicture}

\end{figure}

No obstante, la $V$ puede no estar en el rango $0..255$.
En la versión sin vectorizar comparábamos si no estaba, y, en ese caso, saturábamos el valor.
En esta versión, con la intención de acelerar la transformación, sustituimos las comparaciones
por máximo y mínimo. Si sustituimos saltos condicionales por cálculos mejoraremos el rendimiento,
puesto que si la CPU falla la predicción del salto, el CPI aumentará considerablemente(debemos
tener en cuenta que estamos trabajando con CPUs superescalares, donde se emiten más de una instrucción
por ciclo). La idea es que el mínimo y el máximo no se hagan mediante comparaciones, sino
mediante una instrucción. En este caso, el compilador debe sustituir el mínimo:

\begin{lstlisting}
float min(float a, float b) {
  return (a < b) ? a : b;
}
\end{lstlisting}

por la instrucción \texttt{minss}.

\newpar

Si el valor de $V$ es menor que 0, por ejemplo, $-19$, deberíamos saturarlo a $0$, en cuyo caso:

\begin{equation*}
\begin{aligned}
V \ &= \ min(255,max(0,-19)) \\
V \ &= \ min(255,0) \\
V \ &= \ 0 \\
\end{aligned}
\end{equation*}

Si el valor de $V$ es mayor de 255, por ejemplo, 280, deberíamos saturarlo a 255:

\begin{equation*}
\begin{aligned}
V \ &= \ min(255,max(0,280)) \\
V \ &= \ min(255,280) \\
V \ &= \ 255 \\
\end{aligned}
\end{equation*}

Finalmente, si no tenemos que saturarlo porque se encuentra en el intervalo, por ejemplo,
$100$:

\begin{equation*}
\begin{aligned}
V \ &= \ min(255,max(0,100)) \\
V \ &= \ min(255,100) \\
V \ &= \ 100 \\
\end{aligned}
\end{equation*}

\subsection{Benchmarks}

Primero hacemos una comparativa entre ambos compiladores, para todas las versiones, donde
veremos un detalle interesante de cara al rendimiento. Como entrada, le pasamos una imagen
en formato RGB(en ASCII) de la Mona Lisa, en formato \texttt{ppm}, de 4337x4337 píxeles.

\begin{figure}[H]
\centering

\begin{tikzpicture}

\tikzstyle{every node}=[font=\small]

\begin{axis}[
  tick label style={/pgf/number format/fixed},
  height=6cm,
  width=12cm,
  ymax=0.2,
  ymin=0,
  xtick distance=1,
  %bar shift=0pt,
  %stack plots=y, 
  %area style, 
  enlarge x limits=false,
  symbolic x coords={rgb2yuv1,rgb2yuv2,rgb2yuv2omp,rgb2yuv3,rgb2yuv4},
  ylabel={Tiempo(s)},
]

\addplot+[area legend, fill opacity=0.5,fill=blue!40  ] table [x=Version,y=gcc]  {../ejercicio2/salidas/comparativa/comparativa.csv} \closedcycle;
\addplot+[area legend, fill opacity=0.5,fill=red!40   ] table [x=Version,y=icc]  {../ejercicio2/salidas/comparativa/comparativa.csv} \closedcycle;
\legend{gcc, icc}

\end{axis}

\end{tikzpicture}

\caption{Comparativa entre icc y gcc en el i7-4790K, tiempos en el fichero \textit{comparativa.csv}.}
\label{comparativa}
\end{figure}

Ambos compiladores se han usado con las mismas flags(o equivalentes): Para el \texttt{gcc} usamos 
\texttt{-march=core-avx2 -O2} y para el \texttt{icc} usamos \texttt{-xCORE\_AVX2 -O2}.
Vemos que en la versión sin vectorizar, el \texttt{gcc} obtiene unos tiempos ligeramente mejores
al \texttt{icc}(en este caso, la vectorización automática es mejor en el \texttt{gcc}). Lo realmente interesante
es cuando observamos los resultados en la segunda versión, donde hemos utilizado los \textit{intrinsics} para
vectorizar de forma explícita. La clave de por qué \texttt{icc} gana a \texttt{gcc} en este caso es cómo
han compilado el mínimo y el máximo. Si sacamos el ensamblador de \texttt{gcc}:

\begin{lstlisting}[style=asm]
.L8:
    vmovss  DWORD PTR [rdx+8+rax*4], xmm1
    vextractps      DWORD PTR [rdx+rax*4], xmm0, 3
    vextractps      DWORD PTR [rdx+4+rax*4], xmm0, 2
    add     rax, 3
    cmp     rax, rcx
    jge     .L14
.L7:
    vbroadcastss    ymm2, DWORD PTR [rsi+4+rax*4]
    vbroadcastss    ymm0, DWORD PTR [rsi+8+rax*4]
    vmulps  ymm2, ymm2, ymm5
    vmovaps ymm1, ymm0
    vbroadcastss    ymm0, DWORD PTR [rsi+rax*4]
    vfmadd132ps     ymm1, ymm6, ymm7
    vfmadd132ps     ymm0, ymm2, ymm4
    vaddps  ymm0, ymm0, ymm1
    vextractf128    xmm0, ymm0, 0x1
    vshufps xmm1, xmm0, xmm0, 85
    vcomiss xmm3, xmm1
    jbe     .L15
    vxorps  xmm1, xmm1, xmm1
    jmp     .L8
\end{lstlisting}

A simple vista no somos capaces de determinar dónde se hace el mínimo y el máximo.
Como se comentó en el apartado~\ref{impej2}, la implementación de la función máximo y
mínimo está pensada para ser sustituida por una instrucción como \texttt{minss}:

\begin{lstlisting}[style=asm]
vbroadcastss ymm8, DWORD PTR [8+rsi+rcx*4]              
vbroadcastss ymm6, DWORD PTR [4+rsi+rcx*4]              
vbroadcastss ymm7, DWORD PTR [rsi+rcx*4]                
vfmadd213ps ymm8, ymm2, ymm5                            
vfmadd231ps ymm8, ymm6, ymm3                            
vfmadd231ps ymm8, ymm7, ymm4                            
vmovups   YMMWORD PTR [-32+rsp], ymm8                   
mov       edi, DWORD PTR [-4+rsp]                       
mov       r8d, DWORD PTR [-8+rsp]                       
mov       DWORD PTR [rdx+rcx*4], edi                    
mov       DWORD PTR [4+rdx+rcx*4], r8d                  
vmaxss    xmm9, xmm0, DWORD PTR [-12+rsp]               
vminss    xmm10, xmm1, xmm9                             
vmovss    DWORD PTR [8+rdx+rcx*4], xmm10                
add       rcx, 3                                        
cmp       rcx, rax                                      
jl        ..B3.3 
\end{lstlisting}

Además, la ordenación de las instrucciones del \texttt{icc} parece más apropiada, ya que
está situando las instrucciones de \texttt{fma} seguidas, consiguiendo así ``ocultar'' la
latencia de esta unidad funcional(no de forma total, pues lo normal es que sea de 5 
ciclos~\cite{fmaps}). \newpar

Ahora comparamos el tiempo en las tres máquinas usando el \texttt{icc}, que obtuvo mejor
resultado en la gráfica~\ref{comparativa}.

\begin{figure}[H]
\centering

\begin{tikzpicture}

\tikzstyle{every node}=[font=\footnotesize]

\begin{axis}[
  tick label style={/pgf/number format/fixed},
  %scale only axis,
  height=7cm,
  width=11cm,
  ymax=0.15,
  ymin=0,
  xtick distance=1,
  ybar, %bar chart
  symbolic x coords={rgb2yuv1,rgb2yuv2,rgb2yuv2omp,rgb2yuv3,rgb2yuv4},
  ylabel={Tiempo(s)},
]

\addplot+[area legend, black,fill=blue!40, postaction={pattern=north east lines}] table [x=Version,y=i7-4790K]  {../ejercicio2/salidas/tiempos.csv};
\addplot+[area legend, black,fill=red!40,  postaction={pattern=north east lines}] table [x=Version,y=i5-6400]  {../ejercicio2/salidas/tiempos.csv};
\legend{i7-4790K, i5-6400}
\end{axis}

\end{tikzpicture}
\label{tiempos2}

\caption{Resumen de las distintas versiones para distintos Intel Core, tiempos en el fichero \textit{tiempos.csv}.}
\end{figure}

En vistas a la gráfica~\ref{tiempos2}, si comparamos los tiempos del \texttt{icc}, tenemos
un \texttt{speedup} con vectorización de $0.120/0.084=1.42x$, con vectorización y \texttt{OpenMP}
tenemos un \texttt{speedup} de $0.120/0.047=2.5x$. Sin embargo, las últimas dos versiones no
ofrecen un mejor rendimiento que \texttt{rgb2yuv2}, pese a que estamos usando de  una forma
más eficiente las unidades vectoriales pues las estamos cargando con más datos. \newpar

Para entender este comportamiento, debemos tener en cuenta que estamos ante una aplicación 
intensiva en datos y que depende muy en gran medida de la memoria. Para calcular un píxel, necesitamos
traer a caché los valores RGB de dicho píxel y, tras calcular la transformación, estos valores
no se volverán a necesitar nunca más; no se cumple el principio de localidad temporal. La caché
se encuentra inmersa en un proceso contínuo de llenado y vaciado, limitando así el rendimiento,el
uso y aprovechamiento de las unidades vectoriales.

\newpage

\section{Elecciones Generales}

\subsection{Breve manual de usuario}
Para compilar, usamos el comando \texttt{make}, lo que generará el ejecutable \texttt{conteo}.
El programa lee los parámetros de la entrada estándar; primero preguntará por el número de 
mesas electorales, después por el número de partidos y finalmente, si queremos los resultados
por la salida estándar:

\begin{lstlisting}[style=term]
[pablo@ampe ejercicio3]$ ./conteo 
Introduzca número de mesas electorales: 9
Introduzca número de partidos políticos: 4
Mostrar datos por pantalla (S/N): S
Resultados: 
             89383     30886     92777     36915
             47793     38335     85386     60492
             16649     41421      2362     90027
             68690     20059     97763     13926
             80540     83426     89172     55736
              5211     95368      2567     56429
             65782     21530     22862     65123
             74067      3135     13929     79802
             34022     23058     33069     98167
Total:      482137    357218    439887    556617
Tiempo de Cálculo: 0.0002''.
\end{lstlisting}

\subsection{Listado de ficheros fuente}

\begin{itemize}
\item conteo.cpp
\item Makefile
\end{itemize}

\subsection{Implementación}

Para implementar este ejercicio, definimos dos arrays, uno donde almacenaremos los votos, y
otro donde almacenaremos los resultados. La versión secuencial es muy simple:

\begin{lstlisting}[
    captionpos=b,
    caption=Versión secuencial(Ejercicio 3)
]
void sumar(float* resultados, float* votos, 
           int mesas, int partidos) {
           
  for (int i = 0; i < mesas; i++) {
    for (int j = 0; j < partidos; j++) {
      resultados[j] += votos[i*partidos + j];
    }
  }
}
\end{lstlisting}

Para la versión multimedia, recorremos las mesas y vamos recorriendo los partidos de ocho
en 8(tenemos 8 \texttt{float}s dentro de un vector de \texttt{AVX}). Si los partidos no
son múltiplo de 8, los que resten se calculan en un bucle sin vectorizar. Dentro del bucle
de los partidos, cargamos en los arrays los ocho valores en una sola operación
(\texttt{\_mm256\_loadu\_ps}) y posteriormente hacemos la suma vectorial de ambos registrtos.
Aprovechando que el resultado se encuentra contiguo en memoria, en el registro \texttt{r2},
y sabiendo que el tipo de datos \texttt{\_\_m256} es un vector de 8 \texttt{float}s, copiamos
el resultado al array de resultados con un \texttt{memcpy}.

\begin{lstlisting}[
    captionpos=b,
    caption=Versión vectorizada(Ejercicio 3)
]
void sumar_AVX(float* resultados, float* votos, 
               int mesas, int partidos) {
               
  __m256 r1;
  __m256 r2;
  int i;
  int j;

  for (i = 0; i < mesas; i++) {
    for (j = 0; (j + 8) < partidos; j+= 8) {
      r1 = _mm256_loadu_ps((float *)&votos[i*partidos+j]);
      r2 = _mm256_loadu_ps((float *)&resultados[j]);
      r2 = _mm256_add_ps(r1,r2);

      memcpy(&resultados[j], &r2, 8*sizeof(float));
    }
    for(j;j<partidos;j++)
      resultados[j] += votos[i*partidos + j];
  }
}
\end{lstlisting}

Se ha probado además a reservar memoria de forma alineada mediante \texttt{aligned\_alloc()}
y usando \texttt{\_mm256\_load\_ps} en vez de \texttt{\_mm256\_loadu\_ps}, que no necesita
que la memoria esté alineada en 32 bits, pero esta modificación no ha tenido impacto en
el rendimiento. \newpar

Para paralelizar la función anterior debemos fijarnos en que estamos acumulando el
resultado en el array \texttt{resultados}; Si simplemente añadimos un \texttt{pragma} 
de \texttt{OpenMP} en el bucle exterior, varios hilos accederían al mismo tiempo a 
la misma posición del array y el resultado obtenido no será correcto. Poner el 
\texttt{pragma} en el bucle interior tampoco parece viable pues \texttt{OpenMP} no
permite hacerlo, debido a la condición de fin del bucle. Es por esto por lo que se opta
a paralelizar el código mediante la librería \texttt{pthreads}. \newpar

Primero definimos una estructura para pasar los datos a los hilos:

\begin{lstlisting}
struct st_hilo {
  int partidos;
  int mesas;
  float* votos;
  float* resultados;
  int tam;
  int indice;
};
\end{lstlisting}

Definimos un array de threads, cuyo tamaño será igual al del número cores de la CPU(valor
que obtenemos con \texttt{omp\_get\_num\_procs()}) Recorremos dicho array rellenando
las estructuras de cada hilo y lanzándolo. La información mas destacable que se le pasa
al hilo es el número de columnas(número de partidos) que debe recorrer(campo \texttt{tam}) y
el índice donde debe comenzar. Si los partidos no son múltiplo del número de hilos, se rellenan
las últimas columnas en el bucle final, después de esperar a que acaben todos los hilos.

\begin{lstlisting}[
    captionpos=b,
    caption=Versión paralelizada(Creación de hilos)
]
void sumar_AVX_pthread(float* resultados, float* votos, 
                       int mesas, int partidos) {
  int n_hilos = omp_get_num_procs();
  pthread_t hilos[n_hilos];
  struct st_hilo st_hilos[n_hilos];

  for (int i = 0; i < n_hilos; i++) {
    st_hilos[i].partidos = partidos;
    ...
    st_hilos[i].tam = partidos/n_hilos;
    st_hilos[i].indice = i * st_hilos[i].tam;

    pthread_create(&hilos[i], NULL, 
                   calcular_partido, &st_hilos[i]);
  }

  for (int i = 0; i < n_hilos; i++) {
    pthread_join(hilos[i], NULL);
  }

  for (int i = 0; i < mesas; i++) {
    for(int j=partidos-(partidos % n_hilos);j<partidos;j++)
      resultados[j] += votos[i*partidos + j];
  }

}
\end{lstlisting}

Dentro de cada hilo encontramos el mismo código que teníamos en la función sin paralelizar,
donde lo único que cambia es el índice y el límite del bucle más interno, que permite
repartir el trabajo entre los hilos.

\begin{lstlisting}
void* calcular_partido(void* args) {
  struct st_hilo* s = (struct st_hilo*)args;
  __m256 r1;
  __m256 r2;
  int j;

  for (int i = 0; i < s->mesas; i++) {
    for (j=s->indice;(j + 8)<(s->indice+s->tam);j+= 8) {
      ...
    }
    for(j;j<(s->indice+s->tam);j++)
      s->resultados[j] += s->votos[i*s->partidos + j];
  }

  return NULL;
}
\end{lstlisting}

\subsection{Benchmarks}

En este caso compilamos con \texttt{gcc}(usando el Makefile correspondiente) pues, en general, hemos obtenido
mejores resultados que con \texttt{icc}.

\begin{figure}[H]
\centering

\begin{tikzpicture}

\tikzstyle{every node}=[font=\footnotesize]

\begin{axis}[
  tick label style={/pgf/number format/fixed},
  %scale only axis,
  height=7cm,
  width=11cm,
  ymax=0.6,
  ymin=0,
  xtick distance=1,
  ybar, %bar chart
  symbolic x coords={vsec,vavx,vpth},
  ylabel={Tiempo(s)},
]

\addplot+[area legend, black,fill=blue!40, postaction={pattern=north east lines}] table [x=Version,y=i7-4790K]  {../ejercicio3/salidas/tiempos.csv};
\addplot+[area legend, black,fill=red!40,  postaction={pattern=north east lines}] table [x=Version,y=i5-6400]  {../ejercicio3/salidas/tiempos.csv};
\legend{i7-4790K, i5-6400}
\end{axis}

\end{tikzpicture}
\label{tiempos2}

\caption{Resumen de las distintas versiones para distintos Intel Core}
\end{figure}

Se pueden ver los detalles de los tiempos en el fichero \textit{tiempos.csv}.

\newpage

En este ejercicio hemos comparado tres versiones del programa todas compiladas con las mismas opciones del compilador (\texttt{-O2 -march=core-avx2 -fopenmp}):
\begin{itemize}
\item \textbf{vsec}: Versión secuencial.
\item \textbf{vavx}: Versión vectorizada.
\item \textbf{vpth}: Versión vectorizada y paralelizada con \textit{pthreads}.
\end{itemize}
En todas las pruebas se han usado los mismos parámetros; 100000 mesas y 10000 partidos.

En la versión \textit{vavx} podemos observar una mejora en el tiempo gracias al uso de las unidades vectoriales
del procesador, obteniendo un \texttt{speedup} de $0.52/0.35 = 1.48x$.

En la versión \textit{vpth} el tiempo mejora gracias a la paralización mediante el uso de \textit{pthreads},
podemos observar que no se obtiene una mejora proporcional al número de hilos de la maquina debido a que
el programa es intensivo en datos. Se obtiene un \texttt{speedup} de $0.52/0.28 = 1.86x$.


\bibliographystyle{plain}
\bibliography{references}


\end{document}
